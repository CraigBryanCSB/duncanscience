from data_models.base import BaseModel, BaseManager


class Experiment(BaseModel):
    __tablename__ = 'Experiment'

    def __init__(
        self,
        rowid=None,
        spring_constant=None,
        sample_rate=None,
        retract_trigger=None,
        temperature=None,
        solvent=None,
        surface=None,
        misc=None,
        name=None,
        noise_ref=None
    ):
        self.rowid = rowid
        self.spring_constant = spring_constant
        self.sample_rate = sample_rate
        self.retract_trigger = retract_trigger
        self.temperature = temperature
        self.solvent = solvent
        self.surface = surface
        self.misc = misc
        self.name = name
        self.noise_ref = noise_ref

    def to_sql_insert(self):
        return (
            """
            INSERT INTO {} (
                spring_constant, sample_rate, retract_trigger, temperature,
                solvent, surface, misc, name, noise_ref
            ) VALUES (
                :spring_constant, :sample_rate, :retract_trigger, :temperature,
                :solvent, :surface, :misc, :name, :noise_ref
            )
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_sql_update(self):
        return (
            """
            UPDATE {} SET
                spring_constant = :spring_constant,
                sample_rate = :sample_rate,
                retract_trigger = :retract_trigger,
                temperature = :temperature,
                solvent = :solvent,
                surface = :surface,
                misc = :misc,
                name = :name,
                noise_ref = :noise_ref
            WHERE rowid = :rowid
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'spring_constant': self.spring_constant,
            'sample_rate': self.sample_rate,
            'retract_trigger': self.retract_trigger,
            'temperature': self.temperature,
            'solvent': self.solvent,
            'surface': self.surface,
            'misc': self.misc,
            'name': self.name,
            'noise_ref': self.noise_ref
        }


class ExperimentManager(BaseManager):
    __modelclass__ = Experiment

    def _find_sql(self, filter_args):
        where_clause_args = []

        if not filter_args:
            raise Exception("Don't use find without args, use list instead")

        if 'name' in filter_args:
            where_clause_args.append("name = ?")

        return "SELECT * FROM {} WHERE {}".format(
            self.modelclass.__tablename__, " AND ".join(where_clause_args)
        )
