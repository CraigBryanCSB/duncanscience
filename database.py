import pandas
import os
import ui
import shutil
import sqlite3
import configs
import numpy as np
from constants import TEMPERATURE as DEFAULT_TEMP
from constants import SPRING_CONSTANT as DEFAULT_SPRING_CONSTANT
from constants import SAMPLE_RATE as DEFAULT_SAMPLE_RATE
from constants import RETRACT_TRIGGER as DEFAULT_RETRACT_TRIGGER

if configs.get('testing') is True:
    from matplotlib import pyplot as plt

class TraceFiles(object):
    
    def __init__(self, point, line, fm_path, dfile, zfile, rfile, timefile=None):
        self.point = point
        self.line = line
        self.fm_path = fm_path
        self.dfile = dfile
        self.zfile = zfile
        self.rfile = rfile
        self.timefile = timefile
        
    def to_sql_dict(self):
        return {
            'point': self.point, 'line': self.line, 'fm_path': self.fm_path,
            'defl_path': self.dfile, 'zsens_path': self.zfile,
            'raw_path': self.rfile, 'time_path': self.timefile
        }

class DataBase(object):

    def __init__(self, data_folder):
        #set up locator variables
        self.data_folder = data_folder
        db_choice, db_exists = self._look_for_db()
        self.db_path = os.path.join(self.data_folder, db_choice)
        print self.db_path
        self.cnx = sqlite3.connect(self.db_path)
        self.Experiment = pandas.read_sql("SELECT * FROM Experiment", con=self.cnx)

       
        if not db_exists:
            fm_folders = self._select_fms()
            for i in fm_folders:
                self.dump_forcemap(i)
                
        self.TraceIndex = pandas.read_sql("SELECT * FROM TraceIndex", con=self.cnx)

        if configs.get('testing'):
            self.get_raw_trace(1,1)



    def _look_for_db(self):
        #see if database file in folder contents
        contents = os.listdir(self.data_folder)
        dbdex = []
        for i in contents:
            if ".db" in i:
                dbdex.append(i)

        #if exactly one file, assume this is the desired file
        if len(dbdex) == 1:
            print "Database %s detected. Connecting to database..." %(dbdex[0])
            return  dbdex[0], True#for now

        #if fewer than one file, create a database using the template
        elif len(dbdex) < 1:
            question="No database detected. Would you like to create a database?"
            options = ['Yes','No']
            answer = ui.choose(question, options)
            if answer == "No":
                exit()
            elif answer == "Yes":
                shutil.copyfile("data_template.db", self.data_folder+"/FMdatabase.db")
                print "Creating database file FMdatabase.db"
                return "FMdatabase.db", False

        #if multiple files, choose one
        elif len(dbdex) > 1:
            question="Multiple databases detected. Which database would you like to use?"
            options = dbdex
            answer = ui.choose(question, options)
            return answer, True


    def _select_fms(self):
        #get all folders which may be force maps
        contents = os.listdir(self.data_folder)
        fm_candidates = []
        for i in contents:
            filepath = self.data_folder + i
            if os.path.isdir(filepath):
                fm_candidates.append(filepath)
        
        #select those which are actually force maps you esire to add
        move_on = False
        while not move_on:
            question = "The following folders have been detected:\n%s\n Do wish to add all of their contents to the database?" %(fm_candidates)
            options  = ["Yes","No"]
            answer = ui.choose(question, options)
            if answer == "Yes":
                move_on = True
            elif answer == "No":
                options = fm_candidates
                question = "Which would you like to remove?"
                answer = ui.choose(question, options)
                del fm_candidates[fm_candidates.index(answer)]
       
        #add contents of force map
        return fm_candidates


    def dump_forcemap(self, fm_folder, experiment_params=None):
        if not experiment_params:
            experiment_params = {
                'spring_constant': DEFAULT_SPRING_CONSTANT,
                'sample_rate': DEFAULT_SAMPLE_RATE,
                'retract_trigger': DEFAULT_RETRACT_TRIGGER,
                'temperature': DEFAULT_TEMP
            }

        experiment_id = self._create_experiment(fm_folder, experiment_params)

        if not experiment_id:
            return

        counter = 1
        for tracefiles in self._tracefiles_todb(fm_folder):
            trace_id = self._create_trace_index(tracefiles, experiment_id)
            self._create_rawdata(trace_id, tracefiles)
            print "Adding file %s of %s to database." %(counter,self.tracecount)
            counter += 1

    def _create_experiment(self, fm_folder, experiment_params):
        if 'name' not in experiment_params:
            experiment_params['name'] = fm_folder 
        if 'solvent' not in experiment_params:
            experiment_params['solvent'] = 'water' 
        if 'surface' not in experiment_params:
            experiment_params['surface'] = 'gold'
        if 'misc' not in experiment_params:
            experiment_params['misc'] = None

        cur = self.cnx.cursor()
        cur.execute(
            "SELECT rowid FROM Experiment where name=:name", experiment_params
        )
        if cur.fetchone():
            print("The experiment {} already exists in the database".format(
                experiment_params['name']
            ))
            return None

        cur = self.cnx.cursor() 
        cur.execute(
            """
            INSERT INTO Experiment
                (name, spring_constant, sample_rate, retract_trigger,
                temperature, solvent, surface, misc)
                VALUES (
                    :name, :spring_constant, :sample_rate, :retract_trigger,
                    :temperature, :solvent, :surface, :misc
                );
            """,
            experiment_params
        )
        self.cnx.commit()

        return cur.lastrowid

    def _create_trace_index(self, tracefiles, experiment_id):
        # TODO create traceindex row, save to db, return row id
        trace_index_data = tracefiles.to_sql_dict()
        trace_index_data['experiment_id'] = experiment_id

        cur = self.cnx.cursor()
        cur.execute(
            """
            INSERT INTO TraceIndex
                (fm_path, defl_path, zsens_path, raw_path, time_path,
                experiment_id, line, point)
                VALUES (
                    :fm_path, :defl_path, :zsens_path, :raw_path, :time_path,
                    :experiment_id, :line, :point
                )
            """,
            trace_index_data
        )
        self.cnx.commit()
        return cur.lastrowid


    def _create_rawdata(self, trace_id, tracefiles):
        # open other files, add them to dataframe, then close the filelist
        # TODO use context managers for open files
        folder = tracefiles.fm_path
        
        dfile = open(os.path.join(folder, tracefiles.dfile))        
        dlines = dfile.readlines()
        dvec = np.asarray(dlines).astype(np.float)
        dfile.close()
        
        zfile = open(os.path.join(folder, tracefiles.zfile))        
        zlines = zfile.readlines()
        zvec = np.asarray(zlines).astype(np.float)
        zfile.close()
 
        rfile = open(os.path.join(folder, tracefiles.rfile))        
        rlines = rfile.readlines()
        rvec = np.asarray(rlines).astype(np.float)
        rfile.close()
        
        tfilename = os.path.join(folder, tracefiles.timefile)
        if os.path.isfile(tfilename):
            tfile = open(os.path.join(folder, tfilename))        
            tlines = tfile.readlines()
            tvec = np.asarray(tlines).astype(np.float)
            tfile.close()
        else:
            tvec = np.empty(len(dvec))
            tvec.fill(np.NaN)

        idvec = np.empty(len(dvec))
        idvec.fill(trace_id)
        trace_mat = np.matrix([dvec, zvec, rvec, tvec, idvec]).transpose()
    
        trace_frame = pandas.DataFrame(trace_mat, columns=["deflection", "zsensor", "raw", "time", "trace_id"])
        trace_frame.to_sql(
            name="RawData", con=self.cnx, if_exists="append", index=False
        )



    def _tracefiles_todb(self, fm_folder):
        allfiles = os.listdir(fm_folder)
        goodfiles = []

        # filelister for files matching the desired title type
        for i in allfiles:
            if i[0:4] == "Line":
                if i[8:13] == "Point":
                    goodfiles.append(i)

        # parse for FM shape
        linenums = []
        pointnums = []
        for i in goodfiles:
            linenums.append(i[4:8])
            pointnums.append(i[13:17])
        linenums = set(linenums)
        pointnums = set(pointnums)
        self.tracecount = len(linenums)*len(pointnums)

        for linenum in linenums:
            for pointnum in pointnums:
                title = "Line{}Point{}".format(linenum, pointnum)
                dfile = "{}Defl.txt".format(title)
                zfile = "{}ZSnsr.txt".format(title)
                rfile = "{}Raw.txt".format(title)
                filelist = [dfile, zfile, rfile]
                if all([
                    os.path.isfile(os.path.join(fm_folder, fname))
                    for fname in filelist
                ]):
                    tfile = os.path.join(fm_folder, "{}Time.txt".format(title))
                    if not os.path.isfile(tfile):
                        tfile = "None"
                    yield TraceFiles(
                        pointnum, linenum, fm_folder, dfile, zfile, rfile,
                        timefile=tfile
                    )


    def get_raw_trace(self, line, point, expid=1):
        
        print self.TraceIndex['experiment_id'].isin([expid])
        print self.TraceIndex['line'].isin([line])
        print self.TraceIndex

      
     
    
   
 


