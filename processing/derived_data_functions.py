import numpy as np


def calculate_time(trace, sample_rate=2000):
    timegap = 1 / np.float(sample_rate)
    length = len(trace.index)
    timevector = np.arange(0, length) * timegap
    trace = trace.assign(time=timevector)
    return trace, np.asarray(timevector)


def filter_retract(trace):
    retract_index = np.argmax(trace['deflection'])
    retract_time = trace['time'][retract_index]
    retract_filter = np.greater(trace['time'], retract_time)
    return retract_filter


# takes a symmetric (approach/retract) curve
def get_noise_ref(yvector, readbuff='auto'):
    if readbuff == 'auto':
        readbuff = len(yvector) / 5
    approach_noise = np.std(yvector[:readbuff])
    retract_noise = np.std(yvector[-readbuff:])
    noise = 0.5 * (approach_noise + retract_noise)
    return noise


# retract setpoint in pN
def contact_invols_calc(trace, sample_rate, spring_constant, retract_setpoint=500, readbuff='auto'):
    if readbuff == 'auto':
        readbuff = len(trace.index) / 5

    # calculate surface region given piezo velocity, spring constant, and sample_rate
    piezo_speed = np.absolute(np.polyfit(trace['time'][:readbuff], trace['zsensor'][:readbuff], 1)[0])
    df_dt = spring_constant * piezo_speed
    ramp_time = (retract_setpoint * 10**-12) / df_dt
    n_surface_points = int(ramp_time * sample_rate)
    retract_point = np.argmax(trace['deflection'])

    # use "assumed safe" points to get two lines (hard surface and off surface) and get the intercept
    n_surface_points /= 2
    surface_points = trace[retract_point:retract_point + n_surface_points]
    vertline = np.polyfit(surface_points['zsensor'], surface_points['deflection'], 1)
    horline = np.polyfit(trace['zsensor'][:readbuff], trace['deflection'][:readbuff], 1)
    interceptx = (vertline[1] - horline[1]) / (horline[0] - vertline[0])

    # get indices beyond the first and last 'zsensor point' determined as intercept
    region = trace['zsensor'][trace['zsensor'] > interceptx].index.values.tolist()
    start, stop = min(region), max(region)
    c1 = trace.index.values > start
    c2 = trace.index.values < stop
    contact_idx = c1 & c2
    invols = np.absolute(vertline[0])

    return contact_idx, invols


def calculate_force(deflection, spring_constant, invols):
    force = deflection / invols
    force *= spring_constant
    return force


def calculate_distance(trace, invols, contact_idx):
    Zo = np.amin(trace['zsensor'][contact_idx])
    distance = -(trace['zsensor'] - Zo)
    distance[contact_idx] += trace['deflection'][contact_idx]
    distance = np.asarray(distance)
    return distance
