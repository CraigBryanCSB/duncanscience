import csv
from matplotlib import pyplot
import configs
import numpy
from math import fabs
from constants import TEMPERATURE
from chain_models import WLC_with_intercept

def plot_event(f, s, event_data):                    
    #calculate the reference values 
    refvector = f[len(f) - configs.get('window_size') * 2:len(f) - 2]
    noise_ref = configs.get('peak_sensitivity') * numpy.std(refvector)


    #set event axis limits so that we can see what's being fit 
    xmin,xmax = -configs.get("event_mindist"),3*event_data.trigger_dist
    ymin,ymax = -1.3*fabs(min(f)), max([f[s.index(event_data.smallest_dist)],10*noise_ref])
    pyplot.axis([xmin,xmax, ymin, ymax]) 
    pyplot.title(event_data.title+": "+event_data.event_type)
        
        
    #generate model data for WLC pull if appropriate
    if event_data.event_type != "PLAT" and event_data.persistence_length > 0: 
        sep_for_fit = s[s.index(event_data.smallest_dist):s.index(event_data.trigger_dist)]
        fit = []
        for i in range(len(sep_for_fit)):
            fit.append(
                -1 * WLC_with_intercept(
                    event_data.persistence_length,
                    event_data.chain_length,
                    event_data.plateau_f,
                    sep_for_fit[i] 
                )
            )
        pyplot.plot(sep_for_fit, fit, color="red",label="WLC fit")
    

    #plot the trace, along with the dataa being fit
    pyplot.plot(s, f, color="blue")
        
    pyplot.plot(
        s[s.index(event_data.smallest_dist):s.index(event_data.trigger_dist)], 
        f[s.index(event_data.smallest_dist):s.index(event_data.trigger_dist)],
        color="purple",
        label="Region Fit"
    )
    #TODO make this save to the proper spot
    pyplot.legend(loc="best")
    if configs.get("save_plot"):
        #pyplot.save()
        pytplot.clf()
    if configs.get("show_plot"):
        pyplot.show()


class LogBook(object):
    def __init__(self, output_file, headers, delimiter='\t'):
        self.headers = headers
        self.open_file = open(output_file, 'w')
        self.writer = csv.DictWriter(
            self.open_file, fieldnames=headers, delimiter=delimiter
        )
        self.writer.writeheader()

    # data is expected to be a single row entry, with a value for each field
    def write(self, data):
        if self.open_file.closed:
            raise IOError('Backing file for logbook already closed')
        data = data.to_dict()
        self.writer.writerow(data)

    def close(self):
        try:
            self.open_file.close()
        except IOError:
            # already closed!
            pass
