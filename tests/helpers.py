import sqlite3
import unittest


class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.cnx = sqlite3.connect(':memory:')
        with open('data_schema.sql') as schema:
            cur = self.cnx.cursor()
            cur.executescript(schema.read())
            self.cnx.row_factory = sqlite3.Row
            self.cnx.commit()

    def tearDown(self):
        self.cnx.close()
