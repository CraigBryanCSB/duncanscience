from math import fabs, ceil
from numpy import std
from scipy import stats
from constants import BOLTZMANN_CONSTANT
from utils import warn_reinventing_wheel



def rough_baseline(data, artifact_buffer=100, baseline_samples=100):
    assert type(data) in (tuple, list), "Rough baseline uses a list"
    assert len(data) > baseline_samples + artifact_buffer, \
        "Baseline data too short for number of samples"

    baseline = data[artifact_buffer:artifact_buffer + baseline_samples]
    base = sum(baseline) / len(baseline)

    return [dp - base for dp in data]


def thermal_correct(
    defl_data, time_data, sample_size=100
):
    # NOTE TO DUNCAN. When I worked on this function, the # of points used
    # on the deflection data was twice as many as on the time data.
    # Is this difference on purpose? I didn't think it was, so I removed it.
    
    # RIPOSTE. I see what you're thinking, but
    # the time difference is just the difference between the 
    # AVERAGE time of the first and second deflection averages.
    # Since there's no noise in the time data, and since the 
    # collection rate is constant, the average should work out to 
    # the point in the time file indexed to halfway along the section
    # of force data being averaged

    assert type(defl_data) in (tuple, list), "delf_data needs to be a list"
    assert type(time_data) in (tuple, list), "time_data needs to be a list"
    assert len(defl_data) > 2 * sample_size, \
        "Not enough data for a correction of desired size"

    startpoints = []
    endpoints = []

    for i in range(sample_size):
        startpoints.append(defl_data[i])
        endpoints.append(defl_data[len(defl_data) - i - 1])

    startavg = sum(startpoints) / len(startpoints)
    endavg = sum(endpoints) / len(endpoints)

    dif = endavg - startavg
    tdif = time_data[-ceil(sample_size/2.0)] - time_data[ceil(sample_size/2.0)]

    slope = dif / float(tdif)

    newdef = []
    for i in range(len(defl_data)):
        correction = slope * time_data[i]
        correct_point = defl_data[i] - correction
        newdef.append(correct_point)

    drift = slope

    return newdef, drift


def double_baseline(defl_data, zsens_data, sample_size=1000):
    '''
    Return two parameter baseline
    '''
    assert type(defl_data) in (tuple, list), "delf_data needs to be a list"
    assert type(zsens_data) in (tuple, list), "zsens_data needs to be a list"
    assert len(defl_data) == len(zsens_data)
    assert len(defl_data) > sample_size * 2
    assert len(zsens_data) > sample_size * 2

    x = []
    y = []
    # TODO validate method of doing this
    # QUESTION FOR CRAIG: does validate have a specific meaning or did you 
    # just want me to check it? I had had a mistake earlier (sort of) but
    # i fixed it. This was the fastest way to add the start and end of the 
    # data series to the group of points being used for baselining.If you 
    # have a better one, feel free to show your stuff.
    # Should we kill this comment?
    
    for i in range(sample_size):
        x.append(zsens_data[i])
        y.append(defl_data[i])
        x.append(zsens_data[-sample_size + i])
        y.append(defl_data[-sample_size + i])

    slope, intercept, rval, pval, eval = stats.linregress(x, y)

    

    newdef = []
    for i in range(len(defl_data)):
        correct = zsens_data[i] * slope + intercept
        newdef.append(defl_data[i] - correct)

    return newdef


def calc_forces(z, d, K):
    # TODO NEEDS LOVE
    # "contact" slope
    thirdhigh = max(d) / 3.0
    dvols = []
    svols = []
    counter = 0
    while d[counter] > thirdhigh:
        dvols.append(d[counter])
        svols.append(z[counter])
        counter += 1

    (
        invols, intercept, r_value, p_value, std_err
    ) = stats.linregress(svols, dvols)

    f = []
    for i in range(len(d)):
        Zc = d[i] / invols
        f.append(Zc * K)

    return f


def calc_distances(Zo, z, d):
    # TODO NEEDS LOVE
    # "contact" slope
    thirdhigh = max(d) / 3.0
    dvols = []
    svols = []
    counter = 0
    while d[counter] > thirdhigh:
        dvols.append(d[counter])
        svols.append(z[counter])
        counter += 1

    (
        invols, intercept, r_value, p_value, std_err
    ) = stats.linregress(svols, dvols)

    sep = []

    for i in range(len(z)):
        zcor = Zo - z[i]

        if z[i] > Zo:
            sep.append(d[i] / invols + zcor)
        elif z[i] <= Zo:
            sep.append(zcor)

    return sep


def find_contact(dret, zret):
    '''
    Use the top third of retract curve, along with final third of curve
    to find intersection
    '''
    # TODO NEEDS LOVE

    thirdhigh = max(dret) / 3.0
    thirdfar = int(len(dret) * 0.66)

    # "contact" slope
    d1 = []
    s1 = []
    counter = 0
    while dret[counter] > thirdhigh:
        d1.append(dret[counter])
        s1.append(zret[counter])
        counter += 1

    # "noncontact" slope
    d2 = []
    s2 = []
    for i in range(thirdfar, len(dret)):
        d2.append(dret[i])
        s2.append(zret[i])

    # slope1, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    a1, b1, r_1, p_1, err1 = stats.linregress(s1, d1)
    a2, b2, r_2, p_2, err2 = stats.linregress(s2, d2)

    Zo = (b2 - b1) / (a1 - a2)

    line1 = []
    line2 = []
    for i in range(len(zret)):
        line1.append(a1 * zret[i] + b1)
        line2.append(a2 * zret[i] + b2)

    return Zo


def seperate_retract(trace, defl):
    '''
    Separates a trace into retract and approach. Returns retract
    '''
    # TODO NEEDS LOVE
    maxpoint = defl.index(max(defl))

    retract = []
    for i in range(maxpoint, len(trace)):
        retract.append(trace[i])

    approach = []
    for i in range(maxpoint):
        approach.append(trace[i])

    return retract, approach


def find_1d_diff(vector):
    difvec = []
    for i in range(len(vector) - 1):
        dif = vector[i + 1] - vector[i]
        difvec.append(dif)
    return difvec


def intercept_WLC_func(p, L, G, s, T):
    # TODO NEEDS LOVE
    return (
        BOLTZMANN_CONSTANT * T / p
    ) * (
        0.25 * (1 / (1 - s / L) ** 2) - 0.25 + s / L
    ) + G


def sum_R2(p, L, G, S, T, F):
    # TODO NEEDS LOVE
    R2 = 0
    for i in range(len(F)):
        R2 += (F[i] - intercept_WLC_func(p, L, G, S[i], T)) ** 2
    return R2


@warn_reinventing_wheel
def find_variance(points):
    # TODO use a library function instead
    mean = sum(points) / len(points)

    variance = 0
    for i in range(len(points)):
        variance += (points[i] - mean) ** 2
    variance = variance / (len(points) - 1)

    return variance


@warn_reinventing_wheel
def find_derivative(y, x):
    dydx = []
    xpair = []
    for i in range(len(x) - 1):
        dy = y[i + 1] - y[i]
        dx = x[i + 1] - x[i]
        dydx.append(dy / dx)
        xpair.append((x[i + 1] + x[i]) / 2.0)
    return dydx, xpair



def check_plateau(f, s, noise_ref, window_size, min_length):
    # TODO remove the f and s
    # noise ref is probably the variance of the last 1/3rd of the curve
    length = False
    flatness = False
    height = False
    # check plateau criteria --> length, flatness, height
    fitslope, fitint, r_value, p_value, std_err = stats.linregress(s, f)
    avgF = sum(f) / len(f)

    if max(s) - min(s) > min_length:
        if s.index(max(s)) - s.index(min(s)) > 2 * window_size:
            length = True
    if fitint < -noise_ref and avgF < -noise_ref * 0.66:
        height = True
    if fabs(fitint - avgF) < noise_ref:
        flatness = True

    # print "length",length,"   flatness",flatness,"   height",height

    if length and flatness and height:
        return True
    else:
        return False


# TODO I think this can be cleaner as a "Pull" class
def detect_pulls(f, s, event_data, min_dist, window_size):

    # initialize loop variables
    f_trunc, s_trunc = [], []
    drop_region = []
    lowest_region = []

    # take two averages of windowsize data, which are windowsize +1 apart
    max_dist = int(0.66 * len(s))
    stdev = std(f[max_dist:len(f) - 1])


    # only take points from event minimum to 2/3ds out from surface
    for i in range(max_dist):
        if s[i] > min_dist:
            f_trunc.append(f[i])
            s_trunc.append(s[i])

    # check to see if sudden drop in force is observed in the region of
    # interest by scanning left to right
    for i in range(len(f_trunc) - 2 * window_size):
        left_avg = sum(
            f_trunc[-i - 2 * window_size:-i - window_size]
        ) / window_size
        right_avg = sum(f_trunc[-i - window_size:-i]) / window_size
        if right_avg - left_avg > 2.5 * stdev:
            detect_index = i
            if detect_index > 2 * window_size:
                event_data.presence = True
            break

    # try to better specify event horizon
    if event_data.presence:
        f_diff = find_1d_diff(
            f_trunc[-detect_index - 2 * window_size:-detect_index]
        )
        try:
            drop_point = -detect_index - 2*window_size + f_diff.index(
                max(f_diff)
            )
        except ValueError:
            # TODO move plotting to output module
            print f_diff

        lowest_point = f_trunc.index(
            min(f_trunc[-detect_index - 2 * window_size:-detect_index])
        )

        lowest_region = [s.index(min(s_trunc)), s.index(s_trunc[lowest_point])]
        drop_region = [s.index(min(s_trunc)), s.index(s_trunc[drop_point])]

    event_data.wlc_reg = lowest_region
    event_data.plat_reg = drop_region

    return event_data
