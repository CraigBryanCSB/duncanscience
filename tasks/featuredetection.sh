#!/bin/bash

exptfolder=/media/deshthedesher/Data/science/AFM/2017.03.22_PS_cograft
database=$exptfolder/HR_00database.db
forcemapfolder=$exptfolder/hres00_ascii
exptid=1

python search_whole_map.py $database $exptid
rm *.pyc
rm ../*.pyc
