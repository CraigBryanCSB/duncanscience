Database Clarifications:

***Experiment***
A table outlining path to, inputs for, and metadata regarding, each force map. (each row is one force map). Assume human-editin using dbbrowser whn appropriate.
Fields:
	spring_constant - the measured or given spring constant for the cantilever used on that force map
	sample_rate - the frequency with which datapoints were measured in each force trace (in Hz)
	retract_trigger - the value given to the AFM commanding piezo to begin retracting
	temperature - the temperature at which the measurement was taken (in celcius)
	solvent - the solvent used for the measurement
	surface - information on the sample used for the measurement
	misc - any other information potentially relevant to the force map

***TraceIndex***
A table outlining unique identifiers for each force trace within a given force map,
allowing fast retrieval of data from raw and processed data tables.
Fields:
	fm_path - string with path to folder containing force map 
	defl_path - string with path to original deflection file	
	zsens_path - string with path to original zsenz file
	raw_path - string with path to original raw file
        time_path - string with path to original time file 
	trace_id - unique alphanumeric indentifying the force trace
	line - force map "row" or "y coordinate"
	point - force map "column" or "x coordinate"

***RawData***
A table containing all of the data from the raw data files
Fields:
	deflection - a column containing all deflection data from all Defl files
	zsensor - a column containing all deflection data from all ZSens files
	raw - a column containing all deflection data from all Raw files
	time - a column containing all deflection data from all Time files


***ProcessedData***
A table containing all of the processed data from the raw datafiles
Fields:	
	defl_drift_corrected - deflection values corrected for thermal drift
	delf_interference_corrected - deflection values corrected by an unphysical "limousine" function
	distance - tip-sample separation values calculated using force, znens, and invols
	force - forces experienced by the tip, calculated using defl, invols, and spring constant values
	time - times, either given by timefile or sample rate * datapointindex
	retract_filter - allows quick filtering for approach or retract data. 0 = approach, 1 = retract
	

***TraceOutput***
The ultimate "goal" of this whole thing. contains all (most) of the outputs a human cares 
about from the experiment.
	trace_id - unique alphanumeric indentifying the force trace
	invols_micro - measured invols for that force trace
	invols_meso - averaged invols for nearby force traces
	drift_micro - measured thermal drift for that force trace
	drift_meso - averaged thermal drift for nearby force traces
	indent_detect - binary value indicating if an indentation is present
	indent_mag - float indicating the indentation force
	pull_detect - binary value indicating if a pull is present
	pull_type - string indicating the measured pull type
	pull_force - float giving the measured force (max for WLC, avg for plateau)
	pull_dist - float indicating the distance the pull was detected at


