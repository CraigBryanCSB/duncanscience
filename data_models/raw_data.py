from data_models.base import BaseModel, BaseManager


class RawData(BaseModel):
    __tablename__ = 'RawData'

    def __init__(
        self,
        rowid=None,
        trace_id=None,
        deflection=None,
        zsensor=None,
        raw=None,
        time=None
    ):
        self.rowid = rowid
        self.trace_id = trace_id
        self.deflection = deflection
        self.zsensor = zsensor
        self.raw = raw
        self.time = time

    def to_sql_insert(self):
        return (
            """
            INSERT INTO {} (
                trace_id, deflection, zsensor, raw, time
            ) VALUES (
                :trace_id, :deflection, :zsensor, :raw, :time
            )
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_sql_update(self):
        return (
            """
            UPDATE {} SET
                trace_id = :trace_id,
                deflection = :deflection,
                zsensor = :zsensor,
                raw = :raw,
                time = :time
            WHERE rowid = :rowid
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'trace_id': self.trace_id,
            'deflection': self.deflection,
            'zsensor': self.zsensor,
            'raw': self.raw,
            'time': self.time
        }


class RawDataManager(BaseManager):
    __modelclass__ = RawData

    def _find_sql(self, filter_args):
        if not filter_args:
            raise Exception("Don't use find without args, use list instead")

        where_clause_args = []
        for key in filter_args:
            argstring = key + "=" + str(filter_args[key])
            where_clause_args.append(argstring)

        args_for_query = where_clause_args[0]
        if len(where_clause_args) > 1:
            for i in where_clause_args[1:]:
                args_for_query += " AND {}".format(i)

        return "SELECT * FROM {} WHERE {}".format(
            self.__modelclass__.__tablename__, args_for_query
        )

    def find_by_trace_id(self, trace_id):
        return self.find({'trace_id': trace_id})
