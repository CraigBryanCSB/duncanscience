import processing.derived_data_functions as ddf
import processing.cleaning_functions as cf
import data_models.processed_data
import data_models.trace_output
import data_models.experiment
import pandas
import numpy as np


class RawDataProcessor(object):

    def __init__(self, database_connection):
        self.cnx = database_connection

        # TODO do all this connection handling through manager classes
        self.EM = data_models.experiment.ExperimentManager(self.cnx)
        self.PDM = data_models.processed_data.ProcessedDataManager(self.cnx)
        self.TOM = data_models.trace_output.TraceOutputManager(self.cnx)

    def process_experiment(self, expid):
        # TODO do this through manager classes
        self.Experiment = pandas.read_sql("SELECT * FROM Experiment", con=self.cnx)
        self.TraceIndex = pandas.read_sql(
            "SELECT * FROM TraceIndex WHERE experiment_id={}".format(expid), con=self.cnx
        )

        totalpoints = len(self.TraceIndex.index)
        expid_filter = self.Experiment['rowid'].isin([float(expid)])
        self.sample_rate = self.Experiment['sample_rate'][expid_filter]
        self.spring_constant = self.Experiment['spring_constant'][expid_filter]
        self.ret_set = float(self.Experiment['retract_trigger'][expid_filter])
        for i, trace_id in enumerate(self.TraceIndex['rowid']):
            if i and i % 25 == 0:
                print("Processed {} traces (of {})".format(trace_id - 1, totalpoints))
            raw_trace = pandas.read_sql(
                "SELECT * FROM RawData WHERE trace_id={}".format(trace_id), con=self.cnx
            )
            self.process_raw_trace(trace_id, raw_trace)

    def process_raw_trace(self, trace_id, raw_trace):
        # initialize outputs
        pt = data_models.processed_data.ProcessedData(trace_id=trace_id)
        to = data_models.trace_output.TraceOutput(trace_id=trace_id)

        # touch up raw data
        if pandas.isnull(raw_trace['time'][0]):
            raw_trace, time = ddf.calculate_time(raw_trace, sample_rate=self.sample_rate)
        raw_trace, to.aberrant = cf.clip_data(raw_trace)

        if not to.aberrant:
            # determine approach/retract, contact regions
            pt.retract_filter = ddf.filter_retract(raw_trace)
            pt.contact_filter, to.raw_invols = ddf.contact_invols_calc(
                raw_trace, self.sample_rate, self.spring_constant, self.ret_set
            )

            # perform corrections to data
            o1_correct_trace = cf.correct_o1_slope(raw_trace)
            drift_corrected_trace, pt.defl_drift_corrected, to.drift_micro = cf.drift_correction(o1_correct_trace)
            dummy, to.invols_micro = ddf.contact_invols_calc(
                drift_corrected_trace, self.sample_rate, self.spring_constant, self.ret_set
            )

            pt.defl_interference_corrected = cf.boxcar_spline_correction(
                drift_corrected_trace, pt.contact_filter, pt.retract_filter
            )

            # calculate derived values
            pt.force = ddf.calculate_force(
                pt.defl_drift_corrected, self.spring_constant, to.raw_invols
            )
            pt.force_interference_corrected = ddf.calculate_force(
                pt.defl_interference_corrected, self.spring_constant, to.invols_micro
            )
            pt.distance = ddf.calculate_distance(drift_corrected_trace, to.invols_micro, pt.contact_filter)
            pt.time = np.asarray(o1_correct_trace['time'])
            to.noise_ref = ddf.get_noise_ref(pt.force_interference_corrected)

            self.PDM.create_as_dataframe(pt)

        self.TOM.create(to)
