'''
Runs Duncan's analysis on some data.
Note, if no --output option is specified, no data will be save, the analysis
will just be run.

Usage:
    main.py <data_folder> [options]

Options:
-h --help                       Print this usage.
--config_file=<config_file>     Config file to use [default: default.json].
--fmap_limit=<fmap_limit>       Allows you to limit the number of force curves
                                analysed
'''
from docopt import docopt
from database import DataBase
from configs import get_config

#from runnerfile import AnalysisRunner


if __name__ == '__main__':
    args = docopt(__doc__)

    #config_file = args.get('--config_file') or 'default.json'
    #output_file = args['--output']
    data_folder = args['<data_folder>']
    DB = DataBase(data_folder)
    #fmap_limit = args['--fmap_limit']

    #try:
    #    config = get_config(config_file)
    #except Exception as e:
    #    print "Cannot load specified configuration file: {0}. {1!r}".format(
    #        config_file, e
    #    )

    #runner = AnalysisRunner(
    #    input_folder, output_file=output_file, limit=fmap_limit
    #)
    #runner.run()