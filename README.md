#V02

###FORCE-VOLUME MAPPING###
The experiment is a force-volume mapping experiment. Brief explanation:
A nm-size probe at the end of a soft sping is pushed into, then pulled away
from a surface. This is done multiple times in a raster-scan pattern. 
All the data collected from a single push-pull cycle is called a 'force trace'.

Each FORCE TRACE has 3 or 4 files associated with it. Anatomy of a force trace:
	- deflection file - this contains the signal data, basically how coiled the spring is
	- zsensor file - this how "high" above the surface the instrument thinks the base of the spring is
	- raw file - this is how "high" above the surface the base of the spring should be, given the voltage
	- time file (optional) - this is the time each datapoint in each file was taken

FORCE TRACES are usually represented as a 2D plot, with deflection on the Y axis and 
zsensor on the X axis. In context of a FORCE MAP, all of the data in a FORCE TRACE can 
be thought of as existing on the Z axis.

A force-volume map (usually just called a FORCE MAP) is composed of many FORCE TRACES, 
acquired in a raster-scan pattern. If we imagine that we are looking top-down at a surface
which is being force mapped, the (unfortunate) naming convention is as follows:
	- Line - Row or Y coordinate of a given force trace
	- Point - Column or X coordinate of a given force trace

 
###EXPERIMENT###
The present experiment involes force mapping a surface with polymer chains grafted to it.
The experiment takes place in water. What we are looking for is force traces where a polymer
adsorbs to the tip, and we pull it away from the surface. This will look like a depression
is seen in the signal of the retraction portion of a force trace. It will reveal information on how
polystyrene (a hydrophobic polymer) interacts with itself in water.

Once force traces associated with pulling events are found, we want to measure them. 
We need to determine what type of model best fits these events, and we want to extract 
the parameters associated with that model. I'm currently expecting all pulls to be well
measured by one of the following models:
	- Entropic stretching (Worm-Like Chain or WLC model)
	- Force Plateau (Constant force, reduced from baseline)
	- Joint model (Entropic stretching in high force regime, Plateau in low force)
	- Zhulina model (essentially a joint model, with a slight slope to the plateau)


###DATABASE STRUCTURE###
The database is contains the following tables:
	- Experiment - contains indexing and metadata for each force map
	- TraceOutput - contains any measured values of interest for a given force trace
	- RawData - contains all of the raw data
	- ProcessedData - contains processded raw data
	- TraceIndex - contains all the info to map one table to another. 

Clarification of each field in the database can be found in help.txt


###IMPLEMENTATION NOTES###
to test: $ nosetests tests



###BUILD NOTES###
put stuff here if it's actually useful.











