import os
import configs
from constants import SPRING_CONSTANT
from curve import Curve
from data_structures import AnalyzedDataRow
from output import LogBook
from logreader import AllCurveResults 
from interference import *
from matplotlib import pyplot as plt


class AnalysisRunner(object):
    def __init__(self, input_folder, output_file=None, limit=None, **kwargs):
        self.input_folder = input_folder
        self.limit = limit

        self.logbook = None
        if output_file:
            self.logbook = LogBook(output_file, AnalyzedDataRow.HEADERS)

        # # P.L.O.
        # # NOTE TO DUNCAN
        # # This puts the configurations right on the runner object
        # # allowing them to called like self.config_name
        # # this is not ideal, it would be better to have a list of explicitly
        # # defined variables, but this is quick and easy
        # for key, value in kwargs.items():
        #     setattr(self, key, value)

    def run(self):

        if configs.get('allcurves'):

            # initialize allcurve-only structures
            fmap_names = self._make_fm()
            self.templog = LogBook("templog.csv", AnalyzedDataRow.HEADERS)
          
            ###########################
            # first pass: find events #
            ###########################
            for line_idx, line in enumerate(fmap_names[0:self.limit]):
                for linepoint_idx, linepoint in enumerate(line):

                    title = self._make_title(line_idx, linepoint_idx)
                    print "Analyzing ", title

                    # import a force curve
                    curve = Curve(title, line_idx, linepoint_idx, linepoint, self.input_folder)
                    # zero the curve, fix artifacts
                    curve.clean(
                        configs.get('slope_correct'), configs.get('t_correct')
                    )


                    # calculate force and tip-sample separation
                    curve.calculate_forces_and_separations(SPRING_CONSTANT)

                    # find and record events
                    data_row = curve.calculate_data_row()

                    if data_row.presence:
                        print "Possible Event Found: {}".format(data_row.title)
                        print "determined as a {}".format(data_row.event_type)

                    self.templog.write(data_row)



            ##################################
            # second pass: find interference #
            ##################################

            #filter results for nonevent curves and retrieve associated files
            self.templog.close()
            self.acr = AllCurveResults("templog.csv")
            print "\n***The total fraction of pulls is {}.***\n".format(
                self.acr.count_events()
            )

            nopulllist = self.acr.get_filtered_list("Name","Presence","False")       
            nopullfiles = self._get_filenames(nopulllist)

            #create a master curve representing the interference artifacts 
            if configs.get("allcurves_interference"):
                master_baseline = GlobalInterference() 
                for i in range(len(nopullfiles)):
                    title = nopulllist[i]
                    line, point = self._break_title(nopulllist[i])
                    curve = Curve(title, line, point, nopullfiles[i], self.input_folder)
                    curve.clean(configs.get('slope_correct'),configs.get('t_correct'))
                    curve.calculate_forces_and_separations(SPRING_CONSTANT) 
                
                    master_baseline.add_to_master(curve.force, curve.sep) 
                 
                master_baseline.polish_master()  
                #master_baseline.fit_master_sin()
            
            
            #create a curve representing local interference (drift appropriate)
            if configs.get("local_interference"):
            
                Linereps = []   #stores representative polymonial for each line

                #set up reference data to see when line changes
                currentline, firstpoint  = self._break_title(nopulllist[0])
                line_baseline = GlobalInterference()
                
                #loop over all files without pulls, check if line changes.
                # If line changes, store data and set up new reference.
                for i in range(len(nopullfiles)):
                    title = nopulllist[i]
                    line, point = self._break_title(nopulllist[i])
                    curve = Curve(title, line, point, nopullfiles[i], self.input_folder)
                    curve.clean(configs.get('slope_correct'),configs.get('t_correct'))
                    curve.calculate_forces_and_separations(SPRING_CONSTANT) 
                  
                    if line == currentline:
                        line_baseline.add_to_master(curve.force, curve.sep)
                    
                    elif line != currentline:
                        line_baseline.polish_master()
                        Linereps.append([currentline, line_baseline.polybase])
                        line_baseline = GlobalInterference()
                        currentline = line
                
                #store data from last line
                line_baseline.polish_master()
                Linereps.append([currentline, line_baseline.polybase])




            
                
                    

            ################################
            # third pass: sort and measure #
            ################################
            allpullslist = self.acr.get_filtered_list("Name","Presence","True")
            allpullsfiles = self._get_filenames(allpullslist)
            
            for i in range(len(allpullsfiles)):
                title = allpullslist[i]
                line, point = self._break_title(allpullslist[i])
                curve = Curve(title, line, point, allpullsfiles[i], self.input_folder) 
                curve.clean(configs.get('slope_correct'),configs.get('t_correct'))
                curve.calculate_forces_and_separations(SPRING_CONSTANT) 
                if configs.get("allcurves_interference"):
                    corrected_curve = master_baseline.curve_correct(curve.force, curve.sep) 




        # TODO MOVE TO SINGLE RUNNER FUNCTION
        else:
            the_chosen_one = configs.get("chosencurve")
            line, point = the_chosen_one[0], the_chosen_one[1]
            title = "line%s point%s" % (line, point)

            entry = self._make_single(line, point)

            # import a force curve
            curve = Curve(title, entry, self.input_folder)

            # zero the curve, fix artifacts
            curve.clean(configs.get('slope_correct'), configs.get('t_correct'))

            # calculate force and tip-sample separation
            curve.calculate_forces_and_separations(SPRING_CONSTANT)

            data_row = curve.calculate_data_row()

            # TODO make this better
            print data_row

    def _make_single(self, line, point):
        goodfiles = []
        for i in os.listdir(self.input_folder):
            if i[0:4] == "Line":
                if i[8:13] == "Point":
                    goodfiles.append(i)

        entry = []
        for i in goodfiles:
            if int(i[4:8]) == int(line):
                if int(i[13:17]) == int(point):
                    entry.append(i)

        return entry

    def _make_fm(self):
        # TODO NEEDS LOVE
        # TODO Only works if the full forcemap is imported.
        # It may be desireable to change this.

        files = os.listdir(self.input_folder)
        goodfiles = []
        for i in files:
            if i[0:4] == "Line":
                if i[8:13] == "Point":
                    goodfiles.append(i)

        rows = []
        columns = []
        for i in range(len(goodfiles)):
            rows.append(goodfiles[i][4:8])
            columns.append(goodfiles[i][13:17])

        width = int(max(rows))
        height = int(max(columns))
        print "Importing %s by %s force map." % (width + 1, height + 1)

        fm = []
        for i in range(height + 1):
            row = []
            for k in range(width + 1):
                row.append([])
            fm.append(row)

        for i in range(len(goodfiles)):
            rownum = int(goodfiles[i][4:8])
            colnum = int(goodfiles[i][13:17])
            fm[rownum][colnum].append(goodfiles[i])

        return fm

    def _make_title(self, line, point):
        linenum = ""
        for i in range(4-len(str(line))):
            linenum += "0"
        linenum += str(line)
      
        pointnum = ""
        for i in range(4-len(str(point))):
            pointnum += "0"
        pointnum += str(point)
 
        return "Line%sPoint%s" %(linenum, pointnum)


    def _break_title(self, title):
        #probably no essential, but it helps to speed up code writing
        line = int(title[4:8])
        point = int(title[13:17])
        return line, point

    def _get_filenames(self, namelist):
        file_options = os.listdir(self.input_folder)

        chosen_filenames =[]
        for i in range(len(namelist)):
            linepoint = []
            for j in file_options:
                if namelist[i] in j:
                    linepoint.append(j)
            chosen_filenames.append(linepoint)
        return chosen_filenames        


def combine_BS(f, s, t, avg):
    # TODO move to an appropriate place when implemented
    raise NotImplementedError("Doesn't work yet")
