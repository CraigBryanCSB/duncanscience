import sqlite3
from os import getcwd, remove
from data_dump import DataDump
from raw_data_processor import RawDataProcessor
from feature_detection import FeatureDetection
import unittest


class TestTheTasks(unittest.TestCase):

    def setUp(self):
        # make variables to run tests
        self.working_folder = getcwd()
        self.data_folder = self.working_folder + "/tests/testdata_tailers"
        self.database = self.working_folder + "/tests/testdatabase.db"
        self.expid = 1

    def test_alltasks(self):
        # create db and dump data
        dd = DataDump(self.data_folder, self.database)
        dd.run()

        # reconnect to database
        cnx = sqlite3.connect(self.database)

        # process the data
        rdp = RawDataProcessor(cnx)
        rdp.process_experiment(self.expid)

        # detect things
        fd = FeatureDetection(cnx)
        fd.detect_whole_experiment(self.expid)

    def tearDown(self):
        remove(self.database)
