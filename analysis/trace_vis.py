import seaborn as sns
from matplotlib import pyplot as plt
from data_models.raw_data import RawDataManager
from data_models.processed_data import ProcessedDataManager
import sqlite3


class TraceVis(object):

    def __init__(self, path_to_db):
        self.cnx = sqlite3.connect(path_to_db)
        self.proc_man = ProcessedDataManager(self.cnx)
        self.raw_man = RawDataManager(self.cnx)
        sns.set_style("ticks")

    def plot_processed_deflections(self, trace_id):
        trace = self.proc_man.find_by_trace_id(trace_id)
        x = trace.dist
        y = trace.defl_drift_corrected
        plt.title("Force plot for trace {}".format(trace_id))
        plt.plot(x, y)
        plt.xlabel("tip - sample distance (m)")
        plt.ylabel("deflection")
        plt.show()

    def plot_force(self, trace_id, icorrect=True):
        trace = self.proc_man.find_by_trace_id(trace_id)
        if icorrect:
            y = trace.force_interference_corrected
        else:
            y = trace.force
        x = trace.distance

        plt.title("Force plot for trace {}".format(trace_id))
        plt.plot(x, y)
        plt.xlabel("tip - sample distance (m)")
        plt.ylabel("force")
        sns.despine()
        plt.show()

    def plot_raw_trace(self, trace_id, x="zsensor", y="deflection"):
        trace = self.raw_man.find_by_trace_id(trace_id).to_dataframe()
        plt.title("Raw Plot for Trace {}".format(trace_id))
        plt.plot(trace[x], trace[y])
        plt.ylabel(y)
        plt.xlabel(x)
        sns.despine()
        plt.show()

    def plot_pull(self, trace_id, pull_dist):
        trace = self.proc_man.find_by_trace_id(trace_id).to_dataframe()
        retract_mask = trace['retract_filter'].isin([1])
        contact_mask = trace['contact_filter'].isin([1])
        dist_mask = trace['distance'] <= pull_dist
        pull_mask = retract_mask & ~contact_mask & dist_mask

        x1 = trace['distance'][retract_mask]
        y1 = trace['force_interference_corrected'][retract_mask]

        x2 = trace['distance'][pull_mask]
        y2 = trace['force_interference_corrected'][pull_mask]

        plt.plot(x1, y1, color='blue')
        plt.plot(x2, y2, color='orange')
        sns.despine()
        plt.show()

    def get_connection(self):
        return self.cnx
