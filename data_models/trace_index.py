from data_models.base import BaseModel, BaseManager


class TraceIndex(BaseModel):
    __tablename__ = 'TraceIndex'

    def __init__(
        self,
        rowid=None,
        experiment_id=None,
        fm_path=None,
        defl_path=None,
        zsens_path=None,
        raw_path=None,
        time_path=None,
        line=None,
        point=None
    ):
        self.rowid = rowid
        self.experiment_id = experiment_id
        self.fm_path = fm_path
        self.defl_path = defl_path
        self.zsens_path = zsens_path
        self.raw_path = raw_path
        self.time_path = time_path
        self.line = line
        self.point = point

    def to_sql_insert(self):
        return (
            """
            INSERT INTO {} (
                experiment_id, fm_path, defl_path, zsens_path, raw_path,
                time_path, line, point
            ) VALUES (
                :experiment_id, :fm_path, :defl_path, :zsens_path, :raw_path,
                :time_path, :line, :point
            )
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_sql_update(self):
        return (
            """
            UPDATE {} SET
                experiment_id = :experiment_id,
                fm_path = :fm_path,
                defl_path = :defl_path,
                zsens_path = :zsens_path,
                raw_path = :raw_path,
                time_path = :time_path,
                line = :line,
                point = :point
            WHERE rowid = :rowid
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'experiment_id': self.experiment_id,
            'fm_path': self.fm_path,
            'defl_path': self.defl_path,
            'zsens_path': self.zsens_path,
            'raw_path': self.raw_path,
            'time_path': self.time_path,
            'line': self.line,
            'point': self.point,
        }


class TraceIndexManager(BaseManager):
    __modelclass__ = TraceIndex

    def _find_sql(self, filter_args):
        if not filter_args:
            raise Exception("Don't use find without args, use list instead")

        where_clause_args = []
        for key in filter_args:
            argstring = key + "=" + str(filter_args[key])
            where_clause_args.append(argstring)

        args_for_query = where_clause_args[0]
        if len(where_clause_args) > 1:
            for i in where_clause_args[1:]:
                args_for_query += " AND {}".format(i)

        return "SELECT * FROM {} WHERE {}".format(
            self.__modelclass__.__tablename__, args_for_query
        )

    def find_by_experiment_id(self, experiment_id):
        return self.find({'experiment_id': experiment_id})
