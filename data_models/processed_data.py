from data_models.base import BaseModel, BaseManager


class ProcessedData(BaseModel):
    __tablename__ = 'ProcessedData'

    def __init__(
        self,
        rowid=None,
        trace_id=None,
        force_interference_corrected=None,
        defl_drift_corrected=None,
        defl_interference_corrected=None,
        distance=None,
        force=None,
        time=None,
        retract_filter=None,
        contact_filter=None
    ):

        self.rowid = rowid
        self.trace_id = trace_id
        self.force_interference_corrected = force_interference_corrected
        self.defl_drift_corrected = defl_drift_corrected
        self.defl_interference_corrected = defl_interference_corrected
        self.distance = distance
        self.force = force
        self.time = time
        self.retract_filter = retract_filter
        self.contact_filter = contact_filter

    def to_sql_insert(self):
        return (
            """
            INSERT INTO {} (
                trace_id, defl_drift_corrected, defl_interference_corrected,
                time, distance, force, force_interference_corrected,
                retract_filter, contact_filter
            ) VALUES (
                :trace_id, :defl_drift_corrected, :defl_interference_corrected,
                :time, :distance, :force, :force_interference_corrected,
                :retract_filter, :contact_filter
            )
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_sql_update(self):
        return (
            """
            UPDATE {} SET
                trace_id = :trace_id,
                defl_drift_corrected = :defl_drift_corrected,
                defl_interference_corrected = :defl_interference_corrected,
                time = :time,
                distance = :distance,
                force = :force,
                force_interference_corrected = :force_interference_corrected,
                retract_filter = :retract_filter,
                contact_filter = :contact_filter
            WHERE rowid = :rowid
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'trace_id': self.trace_id,
            'defl_drift_corrected': self.defl_drift_corrected,
            'defl_interference_corrected': self.defl_interference_corrected,
            'time': self.time,
            'distance': self.distance,
            'force': self.force,
            'force_interference_corrected': self.force_interference_corrected,
            'retract_filter': self.retract_filter,
            'contact_filter': self.contact_filter
        }


class ProcessedDataManager(BaseManager):
    __modelclass__ = ProcessedData

    def _find_sql(self, filter_args):
        if not filter_args:
            raise Exception("Don't use find without args, use list instead")

        where_clause_args = []
        for key in filter_args:
            argstring = key + "=" + str(filter_args[key])
            where_clause_args.append(argstring)

        args_for_query = where_clause_args[0]
        if len(where_clause_args) > 1:
            for i in where_clause_args[1:]:
                args_for_query += " AND {}".format(i)

        return "SELECT * FROM {} WHERE {}".format(
            self.__modelclass__.__tablename__, args_for_query
        )

    def find_by_trace_id(self, trace_id):
        return self.find({'trace_id': trace_id})

    def find_with_pandas(self, trace_id):
        command = "SELECT * FROM {}".format(self.__modelclass__.__tablename__)
        conditions = " WHERE trace_id={}".format(trace_id)
        sql = command + conditions
        return self.find_as_dataframe(sql)
