from data_models.base import BaseModel, BaseManager


class TraceOutput(BaseModel):
    __tablename__ = "TraceOutput"

    def __init__(
            self,
            rowid=None,
            trace_id=None,
            raw_invols=None,
            invols_micro=None,
            invols_meso=None,
            drift_micro=None,
            drift_meso=None,
            indent_mag=None,
            indent_detect=None,
            pull_detect=None,
            pull_type=None,
            pull_force=None,
            pull_dist=None,
            noise_ref=None,
            aberrant=None
    ):

        self.rowid = rowid
        self.raw_invols = raw_invols
        self.trace_id = trace_id
        self.invols_micro = invols_micro
        self.invols_meso = invols_meso
        self.drift_micro = drift_micro
        self.drift_meso = drift_meso
        self.indent_mag = indent_mag
        self.indent_detect = indent_detect
        self.pull_detect = pull_detect
        self.pull_type = pull_type
        self.pull_force = pull_force
        self.pull_dist = pull_dist
        self.noise_ref = noise_ref
        self.aberrant = aberrant

    def to_sql_insert(self):
        return (
            """
            INSERT INTO {} (
                trace_id,
                raw_invols,
                invols_micro,
                invols_meso,
                drift_micro,
                drift_meso,
                indent_mag,
                indent_detect,
                pull_detect,
                pull_type,
                pull_force,
                pull_dist,
                noise_ref,
                aberrant
                ) VALUES (
                :trace_id,
                :raw_invols,
                :invols_micro,
                :invols_meso,
                :drift_micro,
                :drift_meso,
                :indent_mag,
                :indent_detect,
                :pull_detect,
                :pull_type,
                :pull_force,
                :pull_dist,
                :noise_ref,
                :aberrant
            )
            """.format(self.__tablename__),
            self.to_dict()
        )

    def to_sql_update(self):
        task = "UPDATE {} SET".format(self.__tablename__)
        entries = " trace_id=?"
        conditions = " WHERE rowid=?"

        values = [self.trace_id[0]]
        for key, value in self.to_dict().iteritems():
            if key not in ['rowid', 'trace_id']:
                entries += ", {}=?".format(key)
                values.append(value[0])
        values.append(self.rowid[0])
        sql = task + entries + conditions

        return sql, values

    def to_dict(self):
        return {
            'rowid': self.rowid,
            'trace_id': self.trace_id,
            'raw_invols': self.raw_invols,
            'invols_micro': self.invols_micro,
            'invols_meso': self.invols_meso,
            'drift_micro': self.drift_micro,
            'drift_meso': self.drift_meso,
            'indent_mag': self.indent_mag,
            'indent_detect': self.indent_detect,
            'pull_detect': self.pull_detect,
            'pull_type': self.pull_type,
            'pull_force': self.pull_force,
            'pull_dist': self.pull_dist,
            'noise_ref': self.noise_ref,
            'aberrant': self.aberrant
        }


class TraceOutputManager(BaseManager):
    __modelclass__ = TraceOutput

    # TODO modify so that SQL injection is safe.
    # note - this will require modifying basemaanger.find()
    def _find_sql(self, filter_args):
        if not filter_args:
            raise Exception("Don't use find without args, use list instead")

        where_clause_args = []
        for key in filter_args:
            argstring = key + "=" + str(filter_args[key])
            where_clause_args.append(argstring)

        args_for_query = where_clause_args[0]
        if len(where_clause_args) > 1:
            for i in where_clause_args[1:]:
                args_for_query += " AND {}".format(i)

        return "SELECT * FROM {} WHERE {}".format(
            self.__modelclass__.__tablename__, args_for_query
        )

    def find_by_trace_id(self, trace_id):
        return self.find({'trace_id': trace_id})
