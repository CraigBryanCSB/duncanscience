PRAGMA foreign_keys = ON;

BEGIN TRANSACTION;
CREATE TABLE "TraceOutput" (
	`rowid`		INTEGER PRIMARY KEY,
	`trace_id`	INTEGER,
	`raw_invols`	INTEGER,
	`invols_micro`	INTEGER,
	`invols_meso`	INTEGER,
	`drift_micro`	INTEGER,
	`drift_meso`	INTEGER,
	`indent_mag`	INTEGER,
	`indent_detect`	INTEGER,
	`pull_detect`	INTEGER,
	`pull_type`	INTEGER,
	`pull_force`	INTEGER,
	`pull_dist`	INTEGER,
        `noise_ref`     INTEGER,
	`aberrant`	INTEGER
);

CREATE TABLE "ProcessedData" (
	`rowid`		INTEGER PRIMARY KEY,
	`trace_id`	INTEGER NOT NULL,
	`defl_drift_corrected`	INTEGER,
	`defl_interference_corrected`	INTEGER,
	`time`	INTEGER,
	`distance`	INTEGER,
	`force`	INTEGER,
	`force_interference_corrected` INTEGER,
	`contact_filter` INTEGER,
	`retract_filter` INTEGER,
	FOREIGN KEY(trace_id) REFERENCES TraceIndex(rowid) ON DELETE CASCADE
);

CREATE TABLE "Experiment" (
	`rowid`		INTEGER PRIMARY KEY,
	`spring_constant`	INTEGER,
	`sample_rate`	INTEGER,
	`retract_trigger`	TEXT,
	`temperature`	INTEGER,
	`solvent`	TEXT,
	`surface`	TEXT,
	`misc`	TEXT,
	`name`	TEXT,
	`noise_ref`	INTEGER
);

CREATE TABLE "TraceIndex" (
	`rowid`		INTEGER PRIMARY KEY,
	`fm_path`	TEXT,
	`defl_path`	TEXT,
	`zsens_path`	TEXT,
	`raw_path`	TEXT,
	`time_path`	TEXT,
	`experiment_id` INTEGER NOT NULL,
	`line`	INTEGER,
	`point`	INTEGER,
	FOREIGN KEY(experiment_id) REFERENCES Experiment(rowid) ON DELETE CASCADE
);

CREATE TABLE "RawData" (
	`rowid`		INTEGER PRIMARY KEY,
	`trace_id`	INTEGER NOT NULL,
	`deflection`	INTEGER,
	`zsensor`	INTEGER,
	`raw`	INTEGER,
	`time`	INTEGER,
	FOREIGN KEY(trace_id) REFERENCES TraceIndex(rowid) ON DELETE CASCADE
);

CREATE INDEX experiment_id_ix ON TraceIndex(experiment_id);
CREATE INDEX proc_trace_id ON ProcessedData(trace_id);
CREATE INDEX raw_trace_id ON RawData(trace_id);

COMMIT;
