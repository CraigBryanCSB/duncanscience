#!/bin/bash

exptfolder=/media/deshthedesher/Data/science/AFM/2017.03.22_PS_cograft
forcemapfolder=$exptfolder/mini
database=$exptfolder/FMdatabase.db

python dump_data_folder_to_db.py $forcemapfolder $database
rm *.pyc
rm ../*.pyc
