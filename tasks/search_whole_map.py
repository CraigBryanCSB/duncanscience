'''
Goes through all processeddate in an experiment, looks for pulls and indentations.

Usage:
    main.py <database> <experiment_id>

Options:
-h --help                       Print this usage.
--expid = <experiment_id>
'''
from docopt import docopt
import sqlite3
from datetime import datetime

from os import getcwd
from sys import path

parentdir = getcwd().replace("/tasks", "/")
path.insert(0, parentdir)

from feature_detection import FeatureDetection

if __name__ == "__main__":
    args = docopt(__doc__)
    database = args['<database>']

    #TODO: make this argument optional, or variable in number
    expid = args['<experiment_id>']

    cnx = sqlite3.connect(database)
    fd = FeatureDetection(cnx)
   
    starttime = datetime.now()
    fd.detect_whole_experiment(expid)
    endtime = datetime.now()

    print(
        "Processing of experiment {} took {} seconds".format(
            expid, (endtime - starttime).seconds
        )
    )

    #TODO spend some time writing timer functions in process_raw if you care
    '''
    print("Spent {} seconds reading files to arrays".format(
        dd.file_reading_time / 1000000.0
    ))
    print("Spent {} seconds prepping for inserts".format(
        dd.prep_time / 1000000.0
    ))
    print("Spent {} seconds inserting data in the database".format(
        dd.insert_time / 1000000.0
    ))
    '''

