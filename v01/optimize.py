from math import fabs, sin, cos, pi
from numpy import negative, polyfit, polyval, fft
import configs
from chain_models import *
from constants import BOLTZMANN_CONSTANT, TEMPERATURE
from matplotlib import pyplot as plt


class WLCFit(object):
    # THIS NEEDS LOVE
    wlc_fitparam = None
    wlc_params = None

    def __init__(self, p_max, p_min, wlc_res_err):
        self.p_min = p_min
        self.p_max = p_max
        self.error_threshold = wlc_res_err

    #TODO figure out why this is a function, and remove it
    def fit(self, f, s):
        cost_history, G, P, L = self.wlc_gradmin(s, negative(f), TEMPERATURE)
        self.wlc_params = [G, P**-1, L]


    def wlc_gradmin(self, Z, F, T):
        #implements gradient-based minimization of WLC function

        #pull config parameters
        max_iter = configs.get('WLC_max_iter')
        stepsize = configs.get('WLC_learn_rate')
        
        #compute initial parameters specific to this force curve
        Lo = max(Z)
        L = 1.5 * max(Z) 
        gradL, G, p_inv = self.wlc_gradL(Z, F, L)


        #set up loop variables
        counter = 0
        precision = 0 
        cost = self.wlc_int_cost(F, p_inv**-1, L, G, Z)
        cost_history = []
        length_history = []


        #Iteratively change the Length parameter until stop condition is met
        stoploop = False
        while stoploop is False:
            tempcost = cost
            gradL, G, p_inv = self.wlc_gradL(Z, F, L)
            dL = gradL * stepsize
            L += dL
            cost = self.wlc_int_cost(F, p_inv**-1, L, G, Z)
            counter += 1
            cost_history.append(cost)

            if (
                L <= Lo or counter > max_iter
                or fabs(tempcost - cost) <= precision
            ):
                stoploop = True
  

            length_history.append(L) 

                
        L = length_history[cost_history.index(min(cost_history))]
        gradL, G, p_inv = self.wlc_gradL(Z, F, L)
 

        return cost_history, G, p_inv, L

    def wlc_gradL(self, Z, F, L):
        #computes the gradient in the cost function with respect to L, the chain length  
        #incidentally computes the optimal values for persitence length P and y-axis offset G      
        T = TEMPERATURE


        kB = BOLTZMANN_CONSTANT
        #initialize loop values required for computing P
        sum_squigP = 0
        sum_squigsquigP = 0
        sum_fsquigP = 0

        #initialize loop values for computing G
        sum_fsquig = 0
        sum_squigsquig = 0
        sum_f = sum(F)
        sum_squig = 0
        N = len(F)

        #Compute sums to determine derived parameters, P and G
        for i in range(len(F)):
            squigP = self.squiggelf_prime(Z[i], L)
            squig = self.squiggelf(Z[i], L)
            sum_squigP += squigP
            sum_squigsquigP += squig * squigP
            sum_fsquigP += F[i] * squigP
            sum_fsquig += F[i] * squig
            sum_squigsquig += squig ** 2
            sum_squig += squig

        #compute derived parameters
        G = (
            sum_fsquig/sum_squigsquig - sum_f/sum_squig
        ) / (
            N / sum_squig - sum_squig / sum_squigsquig
        )
        P_inv = (sum_fsquig + G * sum_squig) / (kB * T * sum_squigsquig)
        # gradL = sum_fsquigP - kB*T*P_inv*sum_squig*sum_squigP + G*sum_squigP

        #compute gradient in L
        gradL = kB * T * P_inv * sum_squigsquigP + G * sum_squigP - sum_fsquigP

        return gradL, G, P_inv

    def wlc_int_cost(self, F, p, L, G, s ):
        #computes cost function for the WLC with intercept model
        cost = 0
        for i in range(len(F)):
            cost += (WLC_with_intercept(p,L,G,s[i]) - F[i])**2
        cost /= float(2*len(F))
        return cost


    def squiggelf(self, z, L):
        #computes segment of WLC function
        return z/L + 0.25 * (1 - z / L) ** -2 - 0.25

    def squiggelf_prime(self, z, L):
        #computes derivative of segment of WLC function
        return (z / (L ** 2)) * (-1 - 0.5 * ((1 - z / L) ** -3))


class SlopeSinFit(object):

    def __init__(self, x, y):
        #get reasonable initial guess values
        linefitparams = polyfit(x, y, 1)
        slope = linefitparams[0]
        intercept = linefitparams[1]

        lineminus = []
        for i in range(len(x)):
            lineminus.append(y[i] - slope*x[i] - intercept)

        self.m = slope
        self.c = intercept
        #self.A = slope
        self.A = ((max(lineminus)-min(lineminus))/2.0)#/((max(x)-min(x))/2.0)
        self.w = 4*pi/(max(x) - min(x))     
        self.B = 0

    def plotsinfunc(self, x, y):
        fox = []
        for i in x:
            fox.append(self.slopesinfunc(i))
        plt.plot(x, y)
        plt.plot(x, fox)
        plt.show()

    def slopesinfunc(self, x):
        fox = (self.A*sin(self.w*x) + self.B*cos(self.w*x)) + x*self.m + self.c
        return fox    
        
    def slopesincost(self, y, fit):
        R2s = []
        for i in range(len(x)):
            R2 = (y[i] - fit[i])**2
            R2s.append(R2)
        cost = sum(R2s)
        return cost



