import sqlite3
from data_models.trace_output import TraceOutputManager
from random import shuffle
from trace_vis import TraceVis


class DatasetVis(object):

    def __init__(self, path_to_db):
        self.cnx = sqlite3.connect(path_to_db)
        self.trac_man = TraceOutputManager(self.cnx)
        self.TV = TraceVis(path_to_db)

    def view_random_traces(self, number=1):
        print('INCOMPLETE')

    def view_random_pulls(self, number=1):
        filter_args = {"pull_detect": 1}
        all_pulls = self.trac_man.find(filter_args).to_dataframe()
        pull_order = all_pulls['trace_id'].tolist()
        shuffle(pull_order)

        for trace_id in pull_order:
            trace_filter = all_pulls['trace_id'].isin([trace_id])
            pull_dist = all_pulls['pull_dist'][trace_filter].values[0]
            # pull_force = all_pulls['pull_force'][trace_filter]
            self.TV.plot_pull(trace_id, pull_dist)

    def view_picky_traces(self, filter_args, number=1):
        print('INCOMPLETE')
