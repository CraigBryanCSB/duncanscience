from analysis_functions import *
import configs
from data_structures import ForceMapData, AnalyzedDataRow
from type_and_measure import TyperAndMeasurer
from matplotlib import pyplot as plt


class Curve(object):
    forces_and_separations = None

    def __init__(self, title, line, point, force_map, input_folder):
        self.data_row = AnalyzedDataRow(title, line, point)
        self.data = self._read_single_force(force_map, input_folder)

    def _read_single_force(self, trace, input_folder):
        tname = None

        for i in trace:
            if "Defl" in i:
                dname = i
            elif "ZSnsr" in i:
                zname = i
            elif "Raw" in i:
                rname = i
            elif "Time" in i:
                tname = i
            else:
            # TODO ERROR HANDLE? BAD DATA???
                print "WARNING: fmap has unexpected number of entries"


        return ForceMapData(input_folder, dname, rname, zname, tname)

    def _maketimefile(samplerate, defl):
        time = []
        for i in range(len(defl)):
            time.append(i * 1.0 / samplerate)
        return time

    def clean(self, slope_correct=False, t_correct=False):
        self.data.remove_artifacts()
        self.data.defl = rough_baseline(self.data.defl)

        if t_correct:
            self.data.defl, self.data.drift = thermal_correct(
                self.data.defl, self.data.time
            )

        # rebaseline
        if slope_correct:
            self.data.defl = double_baseline(
                self.data.defl, 
                self.data.zsen, 
                int(0.5*len(self.data.defl)*configs.get('tail_length'))
            )
        else:
            self.data.defl = rough_baseline(self.data.defl)


    def calculate_forces_and_separations(self, spring_constant):
        defl_ret, defl_app = seperate_retract(self.data.defl, self.data.defl)
        zsens_ret, zsens_app = seperate_retract(self.data.zsen, self.data.defl)

        Zo = find_contact(defl_ret, zsens_ret)
        self.sep = calc_distances(Zo, zsens_ret, defl_ret)
        self.force = calc_forces(zsens_ret, defl_ret, spring_constant) 


    def calculate_data_row(self):
        '''WLC is from the lowest value, plateau is from the lowest region'''

        analyzed_data_row = detect_pulls(
            self.force,
            self.sep,
            self.data_row,
            configs.get('event_mindist'),
            configs.get('window_size')
        )

        # TODO code smell - shouldn't have to reset this title
        analyzed_data_row.title = self.data_row.title

        '''if analyzed_data_row.presence:
            analyzer = TyperAndMeasurer()
            analyzed_data_row = analyzer.run(
                self.forces_and_separations, analyzed_data_row
            )

            # TODO THIS if-else can probably be moved
            if analyzed_data_row.is_plateau():
                trigger_dist = self.forces_and_separations.get_separation(
                    analyzed_data_row.plat_reg[1]
                )
            else:
                # Must be WLC
                trigger_dist = self.forces_and_separations.get_separation(
                    analyzed_data_row.wlc_reg[1]
                )

            analyzed_data_row.trigger_dist = trigger_dist'''

        return analyzed_data_row

   
  

