import configs
import csv

class AllCurveResults:
    def __init__(self, infilename, delimiter='\t'):

        self.infilename = infilename
        self.delimiter = delimiter
        self.results = self.read_results(infilename)
 
    def read_results(self, infilename):
        self.fin = open(infilename, "rb")
        return csv.DictReader(self.fin, delimiter=self.delimiter)

    def update_results(self, newfilename):
        try:
            self.fin.close()
        except IOError:
            print "Previous results file already closed"

        self.results = self.read_results(newfilename)

    def count_events(self):
        #TODO make all small

        total = 0
        events = 0
        with open(self.infilename) as tempfile:
            reader = csv.DictReader(tempfile, delimiter='\t')
            for pull in reader:
                if pull['Presence'] == 'True':
                    events += 1
                total += 1

        if not total:
            pull_fraction = 1
        else:
            pull_fraction = float(events)/float(total)

        return pull_fraction

    
    def get_filtered_list(self, outfield, infield, query):
        list_to_give = []
        for i in self.read_results(self.infilename):
            if query in i[infield]:
                list_to_give.append(i[outfield])
        return list_to_give  
