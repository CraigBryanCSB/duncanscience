import numpy as np


class DropDetector(object):

    def __init__(
        self, force, sep, signal=3, window=20,
        minlength=2.5e-9, readbuff='auto',
        noise_ref='auto'
    ):

        # initialize values
        self.force = force
        self.sep = sep
        self.signal = signal
        self.window = window
        self.readbuff = readbuff
        self.minlength = minlength
        self.longdrop_present = False
        self.spikedrop_present = False

        # assign derived values
        if readbuff == 'auto':
            self.readbuff = len(force) / 3
        if noise_ref == 'auto':
            noise_ref = np.std(force[-readbuff:])
        self.threshold = signal * noise_ref
        self.longdrop_mask = np.zeros(len(self.force), dtype=bool)
        self.spikedrop_mask = np.zeros(len(self.force), dtype=bool)

        # look for drops
        self.force_crawler()

    def force_crawler(self):
        long_drops, spike_drops = [], []
        index = self.readbuff

        while index > 2 * self.window:
            leftindex = index - self.window
            rightindex = index + self.window

            if self.force[leftindex] < self.force[rightindex] - self.threshold:
                dropmask = index_to_mask(
                    length=len(self.force), indices=np.arange(leftindex, rightindex)
                )
                longdropindex, is_long = self.measure_longdrop(dropmask, index)
                spikedropindex, is_spike = self.measure_spikedrop(dropmask, index)
                index = longdropindex[-1] - self.window
                if is_long:
                    long_drops.append(longdropindex)
                if is_spike:
                    spike_drops.append(spikedropindex)
            else:
                index -= 1

        if self.longdrop_present:
            self.longdrop_mask = index_to_mask(
                length=len(self.force), indices=long_drops, dimension=2
            )

        if self.spikedrop_present:
            self.spikedrop_mask = index_to_mask(
                length=len(self.force), indices=spike_drops, dimension=2
            )

    def measure_spikedrop(self, dropmask, trigid):
        depth = int(2.5 * self.window)
        if depth > trigid:
            depth = self.window + 1
        dropstart = trigid - depth + np.argmin(self.force[trigid - depth: trigid])

        index = dropstart
        breakcon1 = False  # must be negative
        breakcon2 = 0  # must be decreasing
        while index > self.window:
            index -= 1
            if self.force[index] > 0:
                breakcon1 = True
            if self.force[index + 1] > self.force[index]:
                breakcon2 += 1
            elif self.force[index + 1] < self.force[index]:
                breakcon2 = 0
            if breakcon1 or breakcon2 > self.signal:
                break

        dropstop, is_spike = index, False
        if dropstart - dropstop > self.signal**2:
            is_spike = True
            self.spikedrop_present = True
        dropidx = np.arange(dropstop, dropstart)

        return dropidx, is_spike

    def measure_longdrop(self, dropmask, trigid):
        dropstart = trigid - self.window + np.argmax(np.diff(self.force)[dropmask[:-1]])
        ref_force = np.average(self.force[dropstart: dropstart + self.window])
        low_force_threshold = ref_force - self.threshold / 2.0

        index = dropstart - self.window + np.argmin(self.force[dropstart - self.window: dropstart - 1])
        while index > self.window:
            if self.force[index] < low_force_threshold:
                index -= 1
            else:
                break

        dropstop, is_long = index, False
        droplength = self.sep[dropstart] - self.sep[dropstop]
        dropidx = np.arange(dropstop, dropstart)
        if droplength > self.minlength:
            avg = np.average(self.force[index_to_mask(length=len(self.force), indices=dropidx)])
            if avg < 0:
                is_long = True
                self.longdrop_present = True

        return dropidx, is_long


def find_contigs(vector):
    # find contiguous regions with drop
    all_contigs = []
    contig = [vector[0]]
    for i in range(1, len(vector)):
        if np.absolute(vector[i] - contig[-1]) == 1:
            contig.append(vector[i])
        else:
            all_contigs.append(contig)
            contig = [vector[i]]
    return all_contigs


def index_to_mask(length, indices, dimension=1):
    mask = np.zeros(length, dtype=bool)

    if dimension == 2:
        for i in indices:
            for j in i:
                mask[j] = 1.0
    elif dimension == 1:
        for i in indices:
            mask[i] = 1.0

    return mask
