import numpy as np
from scipy.interpolate import interp1d


def clip_data(raw_trace, artifact_buffer_size=100, maxreduction=10):
    # reduce the matrix by specified buffer
    frontreduced = raw_trace[artifact_buffer_size:]
    start_distance = frontreduced['zsensor'][artifact_buffer_size + 1]

    # make the distance match up, unless the curve is abberant
    allreduced = frontreduced[frontreduced['zsensor'] > start_distance]

    if len(allreduced.index) < len(raw_trace.index) - maxreduction * artifact_buffer_size:
        allreduced = frontreduced[:len(frontreduced.index) - artifact_buffer_size]
        allreduced.reset_index(drop=True)
        aberrant = True
    else:
        allreduced = allreduced.reset_index(drop=True)
        aberrant = False

    return allreduced, aberrant


def drift_correction(trace, readbuff='auto'):
    if readbuff == 'auto':
        readbuff = len(trace.index) / 4

    # grab regions where there should be no surface effects
    approach_buffer = trace[:readbuff]
    retract_buffer = trace[-readbuff:-2]

    defl_buffer = np.append(approach_buffer['deflection'], retract_buffer['deflection'])
    time_buffer = np.append(approach_buffer['time'], retract_buffer['time'])

    fitparams = np.polyfit(time_buffer, defl_buffer, 0)
    correction_vector = np.polyval(fitparams, trace['time'])
    drift_corrected_defl = np.asarray(trace['deflection'] - correction_vector)
    drift_corrected_trace = trace.assign(deflection=drift_corrected_defl)

    drift = fitparams[-1]
    return drift_corrected_trace, drift_corrected_defl, drift


# boxcar smoothing then cubic spline
def boxcar_spline_correction(trace, surface_filter, retract_filter,  y='deflection', x='zsensor', width='auto'):
    fit_idx = ~surface_filter & ~retract_filter
    tofit = trace[fit_idx]
    fitcount = tofit[y].count()

    # TODO figure out how to compute N based on RSD
    if width == 'auto':
        width = int(fitcount**0.5) / 6
        width = width * 2 + 1

    # represent each data segment by the average of that segment, leaving a buffer of 2 segments
    segments = int(fitcount / float(width))
    ycentroids = np.empty(segments - 2)
    xcentroids = np.empty(segments - 2)

    for i in range(1, segments - 1):
        idx = i * width
        start, stop = idx - (width - 1) / 2, idx + (width - 1) / 2
        ycentroids[i - 1] = np.mean(tofit[y][start:stop])
        xcentroids[i - 1] = np.mean(tofit[x][start:stop])

    # filter data for regions to fit
    minfix, maxfix = np.amin(xcentroids), np.amax(xcentroids)
    c1 = fit_idx
    c2 = trace[x] > minfix
    c3 = trace[x] < maxfix
    appfix_idx = c1 & c2 & c3
    app_tofix = trace[appfix_idx]

    c1 = ~surface_filter & retract_filter
    c2 = trace[x] > minfix
    c3 = trace[x] < maxfix
    retfix_idx = c1 & c2 & c3
    ret_tofix = trace[retfix_idx]

    # correct filtered data by cubic splines of the segmented data
    cubics = interp1d(xcentroids, ycentroids, kind='cubic', bounds_error='false')
    app_corrections = cubics(app_tofix[x])
    ret_corrections = cubics(ret_tofix[x])
    corrected_app_y = app_tofix[y] - app_corrections
    corrected_ret_y = ret_tofix[y] - ret_corrections

    # recontruct the curve
    left_out_idx = ~appfix_idx & ~retfix_idx
    left_out_values = np.asarray(trace[y][left_out_idx].dropna())

    fullcorrection = np.empty(trace[y].count())
    fullcorrection.put(np.where(left_out_idx), left_out_values)
    fullcorrection.put(np.where(appfix_idx), corrected_app_y)
    fullcorrection.put(np.where(retfix_idx), corrected_ret_y)

    return fullcorrection


def correct_o1_slope(trace, readbuff='auto'):
    if readbuff == 'auto':
        readbuff = len(trace.index) / 5

    # grab regions where there should be no surface effects
    approach_buffer = trace[:readbuff]
    retract_buffer = trace[-readbuff:-1]

    # calulate average lines for approach and retract
    abuff_fitparams = np.polyfit(approach_buffer['zsensor'], approach_buffer['deflection'], 1)
    rbuff_fitparams = np.polyfit(retract_buffer['zsensor'], retract_buffer['deflection'], 1)

    avg_fitparams = 0.5 * (abuff_fitparams + rbuff_fitparams)
    correction_vector = np.polyval(avg_fitparams, trace['zsensor'])
    corrected_defl = trace['deflection'] - correction_vector
    trace = trace.assign(deflection=corrected_defl)

    return trace


# noise reduction scales as sqrt(smoothwidth)
# TODO determine how to get smoothwidth based on noise
def moving_average_smoother(noisy_vector, smoothwidth=20, readbuff='auto'):
    length = len(noisy_vector)
    halfwidth = smoothwidth / 2
    smooth_vector = np.empty(length)
    dummy_vector = []
    for i in range(halfwidth, len(smooth_vector) - halfwidth):
        dummy_vector.append(np.average(noisy_vector[i - halfwidth:i + halfwidth]))
    smooth_vector[halfwidth:length - halfwidth] = dummy_vector
    smooth_vector[:halfwidth] = np.mean(noisy_vector[:halfwidth])
    smooth_vector[length - halfwidth:] = np.mean(noisy_vector[length - halfwidth:])

    return smooth_vector


def remove_surface_points(distance, yvector, cutoff=10**-8):
    nearest = np.argmin((distance - cutoff)**2)
    truncdist = distance[nearest:len(distance)]
    truncyvec = yvector[nearest:len(yvector)]
    return truncdist, truncyvec


# WARNING: may be an excercise in poor statistics
def get_characteristic_width(vector, readbuff='auto', chunksize=5):
    if readbuff == 'auto':
        readbuff = len(vector) / 5

    sigma = np.std(vector[-readbuff:])
    mu = np.mean(vector[-readbuff:])

    allcounts = []
    for j in range(5):
        # get minimal fraction
        thisbuff = -readbuff * (j + 1) / 5
        in_dev = 0
        count = 0
        for i in vector[thisbuff:thisbuff + 10]:
            count += 1
            if mu - sigma < i < mu + sigma:
                in_dev += 1
        fraction_in_range = float(in_dev) / count

        while count < -thisbuff and not 0.63 < fraction_in_range < 0.73:
            count += 1
            idx = thisbuff + count
            if mu - sigma < vector[idx] < mu + sigma:
                in_dev += 1
            fraction_in_range = float(in_dev) / count
        allcounts.append(count)

    return np.mean(allcounts)
