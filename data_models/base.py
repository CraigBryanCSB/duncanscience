from copy import copy
import pandas
import sqlite3

# TODO: probably just a small thing I can't figure out. I had 2 problems:
# 1. _find_sql wasn't working as written (see other classes for my fix. May be bad fix.)
# 2. _list_results wasn't crashing the program, but it was just creating genetor objects.
#    to clarify, this is what was happening:
#
#    >>> import data_models.trace_output.TraceOutputManager(cnx) as TOM
#    >>> print(TOM.find_by_trace(trace_id))
#    <generator object _list_results at 0x7f324fd17d70>
#
#   I tried re-writing _list_results without yields, but every time I fed
#   a row to from_sql, it would write over the previous object which had
#   been created. I suspect this has to do with my inadequate understanding
#   of generator functions, which seem difficult to troubleshoot.
#
#   The fix I am currently implementing is to import all rows corresponding to
#   hit at once, and turn them into an object using dumcan functions. It works,
#   but I'm sure the original plan was better. So basically the following functions
#   are redundant, so long as we can get the original doing what it's supposed to:
#
#   dumb_sql
#   _matrix_to_model
#   _list_results_for_dumcies


class MissingFieldError(Exception):
    pass


class NoSuchRecordError(Exception):
    pass


class BaseModel(object):

    @classmethod
    def from_sql(cls, db_row):
        new_model = cls()
        return cls._populate_model_from_mapped_values(
            new_model, db_row, "DB query return"
        )

    @classmethod
    def from_dataframe(cls, dataframe, index):
        new_model = cls()
        df_at_index = dataframe[index]
        return cls._populate_model_from_mapped_values(
            new_model, df_at_index, "Dataframe at index {}".format(index)
        )

    @classmethod
    def _populate_model_from_mapped_values(cls, model, mapped, error_prefix):
        for attr in dir(model):
            if attr.startswith('__'):
                continue  # skip builtins/tablename on the object
            if callable(getattr(model, attr)):
                continue  # skip functions on the object
            try:
                setattr(model, attr, mapped[attr])
            except (KeyError, IndexError):
                raise MissingFieldError(
                    "{} has no value '{}'. "
                    "The model class {} expects this value".format(
                        error_prefix, attr, cls.__name__
                    )
                )
        return model

    @classmethod
    def dumb_sql(cls, matrix, fields):
        new_model = cls()
        return cls._matrix_to_model(
            new_model, matrix, fields, "DB query return"
        )

    @classmethod
    def _matrix_to_model(cls, model, matrix, fields, error_prefix):
        for attr in dir(model):
            if attr.startswith("__"):
                continue
            if callable(getattr(model, attr)):
                continue
            try:
                idx = fields.index(attr)
                setattr(model, attr, matrix[idx])
            except (KeyError, IndexError):
                raise MissingFieldError(
                    "{} has no value '{}'. "
                    "The model class {} expects this value".format(
                        error_prefix, attr, cls.__name__
                    )
                )
        return model

    def to_sql_insert(self):
        '''Returns a query with substitutes and a list of substitutions'''
        raise NotImplementedError("Must be implemented in subclass")

    def to_dict(self):
        raise NotImplementedError("Must be implemented in subclass")

    def to_dataframe(self, index=None):
        if index is None:
            index = self.rowid
        model_dict = self.to_dict()

        # TODO(Craig) figure out why not to use from_dict
        # and replace it with something that works.
        return pandas.DataFrame.from_dict(model_dict)

    def __repr__(self):
        return repr(self.to_dict())


class BaseManager(object):

    def __init__(self, cnx):
        self.cnx = cnx
        self.cnx.row_factory = sqlite3.Row

    def get(self, rowid, as_dataframe=False):
        cur = self.cnx.cursor()
        cur.execute(self._get_by_id_sql(), (rowid,))

        result = cur.fetchone()
        if not result:
            raise NoSuchRecordError(
                "No {} with rowid {}".format(
                    self.__modelclass__.__name__, rowid
                )
            )

        model_result = self.__modelclass__.from_sql(result)
        if as_dataframe:
            return model_result.to_dataframe()
        else:
            return model_result

    def list(self, limit=1000, skip=0, as_dataframe=False):
        cur = self.cnx.cursor()
        cur.execute(self._list_sql(), (limit, skip))

        return self._list_results(cur, as_dataframe=as_dataframe)

    def find(self, filter_args, limit=1000, skip=0, as_dataframe=False):
        cur = self.cnx.cursor()
        substitute_args = copy(filter_args)
        substitute_args['limit'] = limit
        substitute_args['skip'] = skip

        cur.execute(self._find_sql(filter_args), substitute_args)

        # TODO figure out yields (see note at top)

        # return self._list_results(cur, as_dataframe=as_dataframe)
        return self._list_results_for_dumcies(cur, as_dataframe=as_dataframe)

    # intended for use in 'table' classes
    def find_as_dataframe(self, sql):
        dataframe = pandas.read_sql(sql, con=self.cnx)
        return dataframe

    def create(self, model):
        cur = self.cnx.cursor()
        query, args = model.to_sql_insert()
        cur.execute(query, args)
        new_id = cur.lastrowid
        self.cnx.commit()

        model.rowid = new_id
        return model

    def create_as_dataframe(self, model):
        dataframe = model.to_dataframe()
        tablename = self.__modelclass__.__tablename__
        dataframe.to_sql(name=tablename, con=self.cnx, if_exists="append", index=False)

        return model

    def update(self, model):
        cur = self.cnx.cursor()
        query, args = model.to_sql_update()
        cur.execute(query, args)
        self.cnx.commit()

        return model

    def delete(self, model):
        cur = self.cnx.cursor()
        cur.execute(self._delete_sql(), (model.rowid,))
        self.cnx.commit()

    def delete_by_id(self, rowid):
        cur = self.cnx.cursor()
        cur.execute(self._delete_sql(), (rowid,))
        self.cnx.commit()

    def _get_by_id_sql(self):
        return "SELECT rowid, * FROM {} WHERE rowid = ?".format(
            self.__modelclass__.__tablename__
        )

    def _list_sql(self):
        return "SELECT rowid, * FROM {} LIMIT ? OFFSET ?".format(
            self.__modelclass__.__tablename__
        )

    def _find_sql(self, filter_args):
        raise NotImplementedError("Must be implemented in subclass")

    def _delete_sql(self):
        return "DELETE FROM {} WHERE rowid = ?".format(
            self.__modelclass__.__tablename__
        )

    def _list_results(self, cursor, as_dataframe=False):
        result_row = cursor.fetchone()
        print(result_row)
        while result_row:
            model_result = self.__modelclass__.from_sql(result_row)
            if as_dataframe:
                yield model_result.to_dataframe()
            else:
                yield model_result
            result_row = cursor.fetchone()

    def _list_results_for_dumcies(self, cursor, as_dataframe=False):
        result_datamatrix = map(list, zip(*cursor.fetchall()))
        result_fields = [column[0] for column in cursor.description]

        model_result = self.__modelclass__.dumb_sql(result_datamatrix, result_fields)

        if as_dataframe:
            return model_result.to_dataframe()
        else:
            return model_result
