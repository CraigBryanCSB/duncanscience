from constants import SAMPLE_RATE
import csv
from analysis_functions import (
    seperate_retract, find_contact, calc_distances, calc_forces
)

BLANK_TYPE = None
PLATEAU_TYPE = 'PLAT'
UNKNOWN_TYPE = 'U/K'
WLC_TYPE = 'WLC'
COMB_TYPE = 'COMB'


class ForceMapData(object):
    drift = 0

    def __init__(self, path, dname, rname, zname, tname):
        self.defl = self._import_file(path, dname)
        self.raw = self._import_file(path, rname)
        self.zsen = self._import_file(path, zname)
        self.time = None
        if tname:
            self.time = self._import_file(path, tname)
        else:
            self.time = self._maketimefile(SAMPLE_RATE, self.defl)   



    def _import_file(self, path, filename):
        raw = open(path + "/" + filename)
        fin = raw.read()
        fin = fin.split()
        data = []
        for i in fin:
            data.append(float(i))

        raw.close()
        return data

    def _maketimefile(self, samplerate, defl):
        time = []
        for i in range(len(defl)):
            time.append(i * 1.0 / samplerate)
        return time

    def remove_artifacts(self, artifact_buffer_size=100):
        self.defl = self.defl[artifact_buffer_size:-artifact_buffer_size]
        self.raw = self.raw[artifact_buffer_size:-artifact_buffer_size]
        self.zsen = self.zsen[artifact_buffer_size:-artifact_buffer_size]
        self.time = self.time[artifact_buffer_size:-artifact_buffer_size]

class AnalyzedDataRow(object):
    HEADERS = [
        "Name",
        "Line",
        "Point",
        "Presence",
        "Type",
        "Trigger_Dist",
        "Smallest_Dist",
        "Plateau_F",
        "Persistence_Length",
        "Chain_Length"
    ]

    presence = False
    event_type = BLANK_TYPE
    plateau_f = 0  
    persistence_length = 0
    chain_length = 0
    trigger_dist = 0
    smallest_dist = 0

    # Craig's gut says these probably never have to be stored here
    # Craig is right. I have added "smallestdist" to figure it out.
    #TODO figure out where these are called, then fix that
    wlc_reg = None
    plat_reg = None

    def __init__(self, title, point, line):
        self.title = title
        self.line = line
        self.point = point


    def to_dict(self):
        return {
            self.HEADERS[0]: self.title,
            self.HEADERS[1]: self.line,
            self.HEADERS[2]: self.point,
            self.HEADERS[3]: self.presence,
            self.HEADERS[4]: self.event_type,
            self.HEADERS[5]: self.trigger_dist,
            self.HEADERS[6]: self.smallest_dist,
            self.HEADERS[7]: self.plateau_f,
            self.HEADERS[8]: self.persistence_length,
            self.HEADERS[9]: self.chain_length
        }

    def is_plateau(self):
        return self.event_type == PLATEAU_TYPE

    def __str__(self):
        return str(self.to_dict())


class AllCurveResults(object):
    def __init__(self, infilename, delimiter='\t'):
        self.infilename = infilename
        self.delimiter = delimiter
        self.results = self.read_results(infilename)

    def read_results(self, infilename):
        self.fin = open(infilename, "rb")
        return csv.DictReader(self.fin, delimiter=self.delimiter)

    def update_results(self, newfilename):
        try:
            self.fin.close()
        except IOError:
            print "Previous results file already closed"

        self.results = self.read_results(newfilename)

    def count_events(self):
        total = 0
        events = 0
        with open(self.infilename) as tempfile:
            reader = csv.DictReader(tempfile, delimiter='\t')
            for pull in reader:
                if pull['Presence'] == 'True':
                    events += 1
                total += 1

        if not total:
            pull_fraction = 1
        else:
            pull_fraction = float(events)/float(total)

        return pull_fraction
