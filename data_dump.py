from datetime import datetime
import pandas
import os
import sqlite3
import numpy as np
import shutil
from constants import TEMPERATURE as DEFAULT_TEMP
from constants import SPRING_CONSTANT as DEFAULT_SPRING_CONSTANT
from constants import SAMPLE_RATE as DEFAULT_SAMPLE_RATE
from constants import RETRACT_TRIGGER as DEFAULT_RETRACT_TRIGGER


class ForceMapFileReader(object):
    def __init__(self, dfile, zfile, rfile, tfile=None):
        self.dfile = dfile
        self.zfile = zfile
        self.rfile = rfile
        self.tfile = tfile

        self.open_files = []
        self.keys = []

    def open(self):
        self.open_files.append(open(self.dfile, 'r'))
        self.keys.append("deflection")
        self.open_files.append(open(self.zfile, 'r'))
        self.keys.append("zsensor")
        self.open_files.append(open(self.rfile, 'r'))
        self.keys.append("raw")
        if self.tfile:
            self.open_files.append(open(self.tfile), 'r')
            self.keys.append("time")

    def __iter__(self):
        self.open()
        while True:
            lines = [of.readline() for of in self.open_files]
            if not all(lines):
                break
            yield dict(zip(self.keys, [float(l) for l in lines]))
        self.close()

    def close(self):
        for open_file in self.open_files:
            try:
                open_file.close()
            except IOError:
                pass


class TraceFiles(object):

    def __init__(
        self, point, line, fm_path, dfile, zfile, rfile, timefile=None
    ):
        self.point = point
        self.line = line
        self.fm_path = fm_path
        self.dfile = dfile
        self.zfile = zfile
        self.rfile = rfile
        self.timefile = timefile

    def to_sql_dict(self):
        return {
            'point': self.point, 'line': self.line, 'fm_path': self.fm_path,
            'defl_path': self.dfile, 'zsens_path': self.zfile,
            'raw_path': self.rfile, 'time_path': self.timefile
        }


class DataDump(object):

    def __init__(self, data_folder, database_file):
        self.data_folder = data_folder
        self.db_path = database_file
        if not os.path.isfile(self.db_path):
            self._create_database()
        self.cnx = None
        self.tracecount = 0
        self.file_reading_time = 0
        self.insert_time = 0
        self.prep_time = 0


    def run(self):
        self.cnx = sqlite3.connect(self.db_path)
        self.dump_forcemap(self.data_folder)

    def dump_forcemap(self, fm_folder, experiment_params=None):
        if not experiment_params:
            experiment_params = {
                'spring_constant': DEFAULT_SPRING_CONSTANT,
                'sample_rate': DEFAULT_SAMPLE_RATE,
                'retract_trigger': DEFAULT_RETRACT_TRIGGER,
                'temperature': DEFAULT_TEMP
            }

        experiment_id = self._create_experiment(fm_folder, experiment_params)

        if not experiment_id:
            return

        for idx, tracefiles in enumerate(self._get_tracefiles(fm_folder)):
            trace_id = self._create_trace_index(tracefiles, experiment_id)
            self._create_rawdata(trace_id, tracefiles)
            if idx and idx % 25 == 0:
                print "Added {} files (of {}) to the database".format(
                    idx, self.tracecount
                )

    def _create_experiment(self, fm_folder, experiment_params):
        if 'name' not in experiment_params:
            experiment_params['name'] = fm_folder
        if 'solvent' not in experiment_params:
            experiment_params['solvent'] = 'water'
        if 'surface' not in experiment_params:
            experiment_params['surface'] = 'gold'
        if 'misc' not in experiment_params:
            experiment_params['misc'] = None

        cur = self.cnx.cursor()
        cur.execute(
            "SELECT rowid FROM Experiment where name=:name", experiment_params
        )
        if cur.fetchone():
            print("The experiment {} already exists in the database".format(
                experiment_params['name']
            ))
            return None

        cur = self.cnx.cursor()
        cur.execute(
            """
            INSERT INTO Experiment (
                name, spring_constant, sample_rate, retract_trigger,
                temperature, solvent, surface, misc
            ) VALUES (
                :name, :spring_constant, :sample_rate, :retract_trigger,
                :temperature, :solvent, :surface, :misc
            );
            """,
            experiment_params
        )
        self.cnx.commit()

        return cur.lastrowid

    def _create_trace_index(self, tracefiles, experiment_id):
        # TODO create traceindex row, save to db, return row id
        trace_index_data = tracefiles.to_sql_dict()
        trace_index_data['experiment_id'] = experiment_id

        cur = self.cnx.cursor()
        cur.execute(
            """
            INSERT INTO TraceIndex (
                fm_path, defl_path, zsens_path, raw_path, time_path,
                experiment_id, line, point
            ) VALUES (
                :fm_path, :defl_path, :zsens_path, :raw_path, :time_path,
                :experiment_id, :line, :point
            )
            """,
            trace_index_data
        )
        self.cnx.commit()
        return cur.lastrowid

    def _create_rawdata(self, trace_id, tracefiles):
        # open other files, add them to dataframe, then close the filelist
        folder = tracefiles.fm_path

        starttime = datetime.now()
        with open(os.path.join(folder, tracefiles.dfile)) as dfile:
            dvec = np.asarray(dfile.readlines()).astype(np.float)

        with open(os.path.join(folder, tracefiles.zfile)) as zfile:
            zvec = np.asarray(zfile.readlines()).astype(np.float)

        with open(os.path.join(folder, tracefiles.rfile)) as rfile:
            rvec = np.asarray(rfile.readlines()).astype(np.float)

        tfilename = os.path.join(folder, tracefiles.timefile)
        if os.path.isfile(tfilename):
            with open(os.path.join(folder, tfilename)) as tfile:
                tvec = np.asarray(tfile.readlines()).astype(np.float)
        else:
            tvec = np.empty(len(dvec))
            tvec.fill(np.NaN)
        endtime = datetime.now()
        self.file_reading_time += (endtime - starttime).microseconds

        starttime = datetime.now()
        idvec = np.empty(len(dvec))
        idvec.fill(trace_id)

        trace_mat = np.matrix([dvec, zvec, rvec, tvec, idvec]).transpose()

        trace_frame = pandas.DataFrame(
            trace_mat, columns=[
                "deflection", "zsensor", "raw", "time", "trace_id"
            ]
        )
        endtime = datetime.now()
        self.prep_time += (endtime - starttime).microseconds

        starttime = datetime.now()
        trace_frame.to_sql(
            name="RawData", con=self.cnx, if_exists="append", index=False
        )
        endtime = datetime.now()
        self.insert_time += (endtime - starttime).microseconds

    def _get_tracefiles(self, fm_folder):
        allfiles = os.listdir(fm_folder)
        goodfiles = []

        # filelister for files matching the desired title type
        for i in allfiles:
            if i[0:4] == "Line":
                if i[8:13] == "Point":
                    goodfiles.append(i)

        # parse for FM shape
        linenums = set()
        pointnums = set()
        for i in goodfiles:
            linenums.add(i[4:8])
            pointnums.add(i[13:17])
        self.tracecount = len(linenums) * len(pointnums)

        for linenum in linenums:
            for pointnum in pointnums:
                title = "Line{}Point{}".format(linenum, pointnum)
                dfile = "{}Defl.txt".format(title)
                zfile = "{}ZSnsr.txt".format(title)
                rfile = "{}Raw.txt".format(title)
                filelist = [dfile, zfile, rfile]
                if all([
                    os.path.isfile(os.path.join(fm_folder, fname))
                    for fname in filelist
                ]):
                    tfile = os.path.join(fm_folder, "{}Time.txt".format(title))
                    if not os.path.isfile(tfile):
                        tfile = "None"
                    yield TraceFiles(
                        pointnum, linenum, fm_folder, dfile, zfile, rfile,
                        timefile=tfile
                    )
    
    #TODO replace this function with something that creates a database from schema
    def _create_database(self):

        dbtemplate = os.getcwd() + "/dbtemplate.db"
        shutil.copyfile(dbtemplate ,self.db_path)

