docopt==0.6.2
nose==1.3.0
numpy==1.11.2
pandas==0.19.2
scipy==0.17.0
seaborn==0.7.1
