from unittest import TestCase

from analysis_functions import (
    rough_baseline, thermal_correct, double_baseline
)


class TestRoughBaseline(TestCase):
    max_diff = None

    def test_rough_baseline(self):
        data = [
            100.0, 2.3, 2.3, 2.3, 7.3, 10.1, 0.02, 0.01, 0.00, 300
        ]

        result_data = rough_baseline(
            data, artifact_buffer=1, baseline_samples=8
        )

        self.assertEqual(len(result_data), 10)
        self.assertEqual(
            result_data,
            [
                96.95875,
                -0.74125,
                -0.74125,
                -0.74125,
                4.25875,
                7.05875,
                -3.0212499999999998,
                -3.03125,
                -3.04125,
                296.95875
            ]
        )

    def test_rough_baseline_bad_data_type(self):
        data = 123

        with self.assertRaises(AssertionError):
            rough_baseline(data)

    def test_rough_baseline_bad_sample_size(self):
        data = [1, 2, 3]

        with self.assertRaises(AssertionError):
            rough_baseline(data, artifact_buffer=3, baseline_samples=3)


class TestThermalCorrect(TestCase):

    def test_good_data(self):
        defl_data = [2, 2, 50, 50, 60, 20, 6, 6]
        time_data = [1, 2, 3, 4, 5, 6, 7, 8]

        corrected_data, drift = thermal_correct(
            defl_data, time_data, sample_size=2
        )

        self.assertEqual(drift, 1.0)

        self.assertEqual(len(corrected_data), 8)
        for dp in corrected_data:
            self.assertEqual(type(dp), float)
        self.assertEqual(
            corrected_data,
            [
                1.0, 0.0, 47.0, 46.0, 55.0, 14.0, -1.0, -2.0
            ]
        )

    def test_wrong_type_defl(self):
        with self.assertRaises(AssertionError):
            thermal_correct('hello', [1, 2, 3])

    def test_defl_data_too_short(self):
        with self.assertRaises(AssertionError):
            thermal_correct(
                [1, 2, 3],
                [1, 2, 3],
                sample_size=2
            )

    def test_wrong_type_time(self):
        with self.assertRaises(AssertionError):
            thermal_correct([1, 2, 3], 'hello')

    def test_time_data_too_short(self):
        with self.assertRaises(IndexError):
            thermal_correct(
                [1, 2, 3, 4, 5, 6, 7, 8],
                [1, 2, 3, 4],
                sample_size=3
            )


class TestDoubleBaseline(TestCase):

    def test_good_data(self):
        # TODO - actually make test
        pass

    def test_wrong_type_delf(self):
        with self.assertRaises(AssertionError):
            double_baseline('hello', [1, 2, 3])

    def test_wrong_type_zsens(self):
        with self.assertRaises(AssertionError):
            double_baseline([1, 2, 3], 'hello')

    def test_differing_length_data(self):
        with self.assertRaises(AssertionError):
            double_baseline([1, 2, 3], [1, 2, 3, 4])

    def test_data_too_short(self):
        with self.assertRaises(AssertionError):
            double_baseline([1, 2, 3], [1, 2, 3])


class TestCalcForces(TestCase):
    pass
