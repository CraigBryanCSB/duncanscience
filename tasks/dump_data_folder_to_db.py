'''
Dumps a single folder of trace data into the specified database

Usage:
    main.py <data_folder> <database>

Options:
-h --help                       Print this usage.
'''
from docopt import docopt
from datetime import datetime

from os import getcwd
from sys import path

parentdir = getcwd().replace("/tasks", "/")
path.insert(0, parentdir)



from data_dump import DataDump


if __name__ == '__main__':
    args = docopt(__doc__)
    data_folder = args['<data_folder>']
    database = args['<database>']

    print "\nDumping data from: \n{}\n to:\n{}\n".format(
            data_folder, database
            )


    dd = DataDump(data_folder, database)

    starttime = datetime.now()
    dd.run()

    endtime = datetime.now()
    print(
        "Datadump of {} took {} seconds".format(
            data_folder, (endtime - starttime).seconds
        )
    )

    print("Spent {} seconds reading files to arrays".format(
        dd.file_reading_time / 1000000.0
    ))
    print("Spent {} seconds prepping for inserts".format(
        dd.prep_time / 1000000.0
    ))
    print("Spent {} seconds inserting data in the database".format(
        dd.insert_time / 1000000.0
    ))
