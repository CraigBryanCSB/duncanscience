#!/bin/bash

exptfolder=/media/deshthedesher/Data/science/AFM/2017.03.22_PS_cograft
database=$exptfolder/FMdatabase.db
exptid=1

python batch_process_rawdata.py $database $exptid
rm *.pyc
rm ../*.pyc
