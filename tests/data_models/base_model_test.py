import pandas

from data_models.base import BaseModel, BaseManager
from tests.helpers import DatabaseTest


class MockModel(BaseModel):
    __table_name__ = 'UnitTestTable'

    def __init__(self, rowid=None, test_field=None):
        self.rowid = rowid
        self.test_field = test_field

    def to_sql_insert(self):
        return "INSERT INTO UnitTestTable (test_field) VALUES ()"

    def to_dict(self):
        return {'rowid': self.rowid, 'test_field': self.test_field}


class TestBaseModel(DatabaseTest):

    def setUp(self):
        DatabaseTest.setUp(self)
        self.cnx.execute("CREATE TABLE UnitTestTable(test_field TEXT);")
        self.cnx.commit()

    def test_from_sql(self):
        self.cnx.execute(
            "INSERT INTO UnitTestTable(test_field) VALUES ('fake');"
        )
        self.cnx.commit()
        cursor = self.cnx.cursor()
        cursor.execute("SELECT rowid, * FROM UnitTestTable;")

        model = MockModel.from_sql(cursor.fetchone())

        self.assertEqual('fake', model.test_field)

    def test_from_dataframe(self):
        dataframe = pandas.DataFrame(
            {1: {'rowid': 52, 'test_field': 'a thing'}}
        )

        model = MockModel.from_dataframe(dataframe, 1)

        self.assertEqual('a thing', model.test_field)
        self.assertEqual(52, model.rowid)

    def to_dataframe(self):
        model = MockModel(rowid=123, test_field='hello world')

        dataframe = model.to_dataframe()
        test_dataframe = pandas.DataFrame(
            {123: {'rowid': 123, 'test_field': 'hello world'}}
        )

        self.assertEqual(test_dataframe, dataframe)

    def to_dataframe_specify_index(self):
        model = MockModel(rowid=124, test_field='hello world2')

        dataframe = model.to_dataframe(index=42)
        test_dataframe = pandas.DataFrame(
            {42: {'rowid': 124, 'test_field': 'hello world2'}}
        )

        self.assertEqual(test_dataframe, dataframe)


class TestBaseModelManager(DatabaseTest):

    def setUp(self):
        DatabaseTest.setUp(self)
        self.cnx.execute("CREATE TABLE UnitTestTable(test_field TEXT);")
        self.cnx.commit()

    def test_get(self):
        pass

    def test_list(self):
        pass

    def test_find(self):
        pass

    def test_create(self):
        pass

    def test_update(self):
        pass

    def test_delete(self):
        pass

    def test_delete_by_id(self):
        pass
