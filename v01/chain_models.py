from constants import BOLTZMANN_CONSTANT, TEMPERATURE




def WLC_with_intercept(p, L, G, s):
    # TODO NEEDS LOVE
    return (
        BOLTZMANN_CONSTANT * TEMPERATURE / p
    ) * (
        0.25 * (1 / (1 - s / L) ** 2) - 0.25 + s / L
    ) + G
