
def warn_reinventing_wheel(func):
    def wrapper(*args, **kwargs):
        print "WARNING: Using a home-grown math function: {}".format(
            func.__name__
        )
        return func(*args, **kwargs)
    return wrapper
