import configs
from matplotlib import pyplot as plt
from numpy import polyfit, polyval
from optimize import SlopeSinFit



class GlobalInterference(object):

    def __init__(self):
        self.masterforce = []
        self.masterdist = []
        self.curvecount = []

        self.truncpoint = 3*configs.get('event_mindist')    

    def polish_master(self):
        for i in range(len(self.masterforce)):
            n = float(self.curvecount[i])
            self.masterforce[i] /= n
            self.masterdist[i] /= n 


        self.polybase = polyfit(self.masterdist, self.masterforce, 9)  

    def curve_correct(self, force, dist):
        trunc_force, trunc_dist = [], []
        for i in range(len(dist)):
            if dist[i] > self.truncpoint:
                trunc_force.append(force[i])
                trunc_dist.append(dist[i])        

        curvebase = polyval(self.polybase, trunc_dist)
        
        correct_forces = []
        for i in range(len(curvebase)):
            corforce = trunc_force[i] - curvebase[i]
            correct_forces.append(corforce)


        return trunc_force, trunc_dist
    
    def fit_master_sin(self):
        ssf = SlopeSinFit(self.masterdist, self.masterforce)
        ssf.plotsinfunc(self.masterdist, self.masterforce)

    def add_to_master(self, force, dist):
        pointnum = int((dist[-1] - 3*configs.get('event_mindist'))/float(configs.get('aggregate_size')))
        self.chunked_force, self.chunked_dist = self.chunk_data(force, dist, pointnum)

        for i in range(len(self.chunked_force)):
            try:
                self.masterforce[i] += self.chunked_force[i]
                self.masterdist[i] += self.chunked_dist[i]
                self.curvecount[i] += 1
            except IndexError:
                self.masterforce.append(self.chunked_force[i])
                self.masterdist.append(self.chunked_dist[i])
                self.curvecount.append(1) 

    #Just Trust me, this makes sense.
    def chunk_data(self, force, dist, chunknum):
        mindex = 0 
        while dist[mindex] <= 3*configs.get("event_mindist"):
            mindex += 1

        force_chunks = []
        brackets = [3*configs.get('event_mindist')]
        for i in range(chunknum):
            brackets.append(3*configs.get('event_mindist') + configs.get('aggregate_size')*(i+1))
            force_chunks.append([])
  

        brackettoggle = 0
        jcount = 1
        for i in range(len(dist)):        

            try:
                force_chunks[jcount-1]
                if brackets[jcount-1] < dist[i] <= brackets[jcount]:
                    force_chunks[jcount-1].append(force[i])
            
                    if brackettoggle < jcount:
                        brackettoggle += 1
                else:
                    if brackettoggle > 0:
                        jcount += 1
                        force_chunks[jcount].append(force[i])
            
            except IndexError:
                pass



        chunked_forces, chunked_dists = [],[]
        for i in range(len(force_chunks)):
            if len(force_chunks[i]) > 0:
                chunked_forces.append(sum(force_chunks[i])/float(len(force_chunks[i])))
                chunked_dists.append((brackets[i]+brackets[i+1])/2.0)            

        return chunked_forces, chunked_dists

class LocalInterference(object):

    def __init__(self):
        self.masterforce = []
        self.masterdist = []
        self.curvecount = []

        self.truncpoint = 3*configs.get('event_mindist')    

       
