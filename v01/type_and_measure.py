'''
NOTE TO DUNCAN
This is a giant function that could use some work, but I've isolated it here
for ease of refactoring. Its not ideal here, but I threw it all in a class to
be able to encapsulate the calls to the configuration.
'''
from math import fabs
from output import plot_event

# TODO (long term): Move pyplot dependence elsewhere
from matplotlib import pyplot

import numpy #TODO import appropriate functions instead of entire class
from analysis_functions import check_plateau, intercept_WLC_func
from constants import TEMPERATURE
import configs
from data_structures import UNKNOWN_TYPE, PLATEAU_TYPE, WLC_TYPE
from optimize import WLCFit


#TODO re-write this whole class as a neural net

class TyperAndMeasurer(object):  # Worst class name.
    # NEEDS MORE WORK - currently pointless as a class
    # (could just be functions)

    def run(self, f_and_s, event_data):
        # these are initialized for compatiablility (for now)
        # TODO remove this and work through this function to replace calls
        WLC_reg = event_data.wlc_reg
        PLAT_reg = event_data.plat_reg
        s = f_and_s.separations
        f = f_and_s.forces
        # TODO DONE REMOVE

        # initialize parameters
        further_event_likely = False
        max_dist = int(0.66 * len(s))  # UNUSED?
        left_trigger = False
        left_event_index = 0
        event_data.event_type = UNKNOWN_TYPE

        refvector = f[len(f) - configs.get('window_size') * 2:len(f) - 2]
        noise_ref = configs.get('peak_sensitivity') * numpy.std(refvector)

        # find the left event index, assuming a plateau event
        # use two-window system, as in event detection
        counter = 0

        # TODO - move guts of this loop to separate function
        while left_trigger is False:
            left_avg = sum(
                f[(
                    PLAT_reg[1] - counter - 2 * configs.get('window_size')
                ):PLAT_reg[1] - counter - configs.get('window_size')]
            ) / configs.get('window_size')

            right_avg = sum(
                f[PLAT_reg[1] - counter - configs.get(
                    'window_size'
                ):PLAT_reg[1] - counter]
            ) / configs.get('window_size')

            if fabs(right_avg - left_avg) > noise_ref:
                left_trigger = True
                # set the index as the center of the left window, or the lowest
                # acceptable value - whichever is closer to the surface
                left_event_index = max([
                    PLAT_reg[1] - counter - int(1.5 * configs.get(
                        'window_size')
                    ),
                    PLAT_reg[0]
                ])
                if right_avg < left_avg:
                    further_event_likely = True
            elif counter > PLAT_reg[1] - PLAT_reg[0]:
                left_event_index = PLAT_reg[0]
                break
            counter += 1

        # check to see if "plateau" passes all plateau criteria:
        if check_plateau(
            f[left_event_index:PLAT_reg[1]],
            s[left_event_index:PLAT_reg[1]],
            noise_ref,
            configs.get('window_size'),
            configs.get('min_plateau_length')
        ):
            event_data.event_type = PLATEAU_TYPE
            event_data.chain_length = s[PLAT_reg[1]]
            event_data.plateau_f = sum(
                f[left_event_index:PLAT_reg[1]]
            ) / (PLAT_reg[1] - left_event_index)

        # if plateau is false, check WLC
        else:
            # re-determine left trigger, assuming WLC shape
            # measure fluctuations in data. if negative flux,
            # it's likeley that the event has begun
            # TODO MOVE THIS ELSE TO OWN FUNCTION
            left_trigger = False
            counter = 0
            strikenum = 0
            while left_trigger is False:
                if f[WLC_reg[1] - counter - 1] < f[WLC_reg[1] - counter]:
                    strikenum += 1
                elif f[WLC_reg[1] - counter -1] > 0:
                    strikenum += 1
                #else:
                #    strikenum *= 0.9

                if strikenum > configs.get('maxstrikes'):
                    left_trigger = True
                    left_event_index = WLC_reg[1] - counter

                elif (
                    f[WLC_reg[1] - counter] - f[WLC_reg[1] - counter - 2] >=
                    0.5*(f[WLC_reg[1] + 2] - f[WLC_reg[1]])
                ):
                    left_trigger = True
                    left_event_index = WLC_reg[1] - counter + 1
                    further_event_likely = True  # TODO UNUSED


                elif s[WLC_reg[1] - counter - 1] <= configs.get("event_mindist"):
                    left_event_index = WLC_reg[1] - counter - 1
                    break

                counter += 1

           


            # determine if it's a WLC event, if there are enough points
            if WLC_reg[1] - left_event_index > configs.get('wlc_minpoints'):
                wlc_fit = WLCFit(
                    configs.get('p_max'),
                    configs.get('p_min'),
                    configs.get('wlc_res_err')
                )

                wlc_fit.fit(
                    f[left_event_index:WLC_reg[1]],
                    s[left_event_index:WLC_reg[1]]
                )
                event_data.plateau_f = wlc_fit.wlc_params[0]
                event_data.persistence_length = wlc_fit.wlc_params[1]
                event_data.chain_length = wlc_fit.wlc_params[2]


                if wlc_fit.wlc_fitparam:
                    event_data.event_type = WLC_TYPE

         
        event_data.trigger_dist = s[WLC_reg[1]]
        event_data.smallest_dist = s[left_event_index] 
        

        if event_data.event_type in configs.get('plot_event_type'):
            plot_event(f,s,event_data)

        return event_data


