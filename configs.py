import json

from constants import (
    CONFIGURATIONS_DIRECTORY, DEFAULT_CONFIG_FILENAME
)

REQUIRED_CONFIGS = ['testing'
]


_CONFIG = None


class UnknownConfigError(Exception):
    pass


def get_config(filename='default.json'):
    configs = {}
    defaults = {}

    if filename != 'default.json':
        with open('{}/{}'.format(
            CONFIGURATIONS_DIRECTORY, DEFAULT_CONFIG_FILENAME
        ), 'r') as f:
            defaults = json.loads(f.read())

    with open('{}/{}'.format(CONFIGURATIONS_DIRECTORY, filename), 'r') as f:
        configs = json.loads(f.read())

    for key in REQUIRED_CONFIGS:
        if configs.get(key) is None:
            configs[key] = defaults.get(key)

    _CONFIG = configs
    return _CONFIG


def get(key):
    global _CONFIG
    if _CONFIG is None:
        _CONFIG = get_config()

    if key not in _CONFIG:
        raise UnknownConfigError("Unrecognized config {}".format(key))
    return _CONFIG.get(key)
