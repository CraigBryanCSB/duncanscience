import json

from constants import (
    CONFIGURATIONS_DIRECTORY, DEFAULT_CONFIG_FILENAME
)

REQUIRED_CONFIGS = [
    'slope_correct', 't_correct', 'window_size', 'peak_sensitivity',
    'maxstrikes', 'allcurves', 'chosencurve',
    'plot_event_type', 'min_plateau_length', 'wlc_res_err', 'p_min', 'p_max',
    'event_mindist','wlc_minpoints','show_plot','save_plot','WLC_max_iter','WCL_learn_rate',
    'tail_length'
]


_CONFIG = None


class UnknownConfigError(Exception):
    pass


def get_config(filename='default.json'):
    configs = {}
    defaults = {}

    if filename != 'default.json':
        with open('{}/{}'.format(
            CONFIGURATIONS_DIRECTORY, DEFAULT_CONFIG_FILENAME
        ), 'r') as f:
            defaults = json.loads(f.read())

    with open('{}/{}'.format(CONFIGURATIONS_DIRECTORY, filename), 'r') as f:
        configs = json.loads(f.read())

    for key in REQUIRED_CONFIGS:
        if configs.get(key) is None:
            configs[key] = defaults.get(key)

    _CONFIG = configs
    return _CONFIG


def get(key):
    global _CONFIG
    if _CONFIG is None:
        _CONFIG = get_config()

    if key not in _CONFIG:
        raise UnknownConfigError("Unrecognized config {}".format(key))
    return _CONFIG.get(key)
