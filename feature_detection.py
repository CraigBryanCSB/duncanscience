import data_models.processed_data
import data_models.trace_output
import data_models.trace_index
import processing.cleaning_functions as cleaners
from processing.detector_functions import DropDetector
import numpy as np


class FeatureDetection(object):

    def __init__(self, database_connection):
        self.cnx = database_connection
        self.TOM = data_models.trace_output.TraceOutputManager(self.cnx)
        self.PDM = data_models.processed_data.ProcessedDataManager(self.cnx)
        self.TIM = data_models.trace_index.TraceIndexManager(self.cnx)

    def detect_whole_experiment(self, expid):

        AllTraceIndices = self.TIM.find_by_experiment_id(expid)
        tracetotal = len(AllTraceIndices.rowid)

        noise_values = np.empty(tracetotal)
        for i, rowid in enumerate(AllTraceIndices.rowid):
            TraceOutput = self.TOM.find_by_trace_id(rowid)
            if not TraceOutput.aberrant[0]:
                noise_values[i] = TraceOutput.noise_ref[0]
        self.mean_noise = np.mean(noise_values)

        for trace_id in AllTraceIndices.rowid:

            if trace_id % 25 == 0:
                print("Searching for pulls in trace {} (of {})".format(trace_id, tracetotal))
            self.TraceOutput = self.TOM.find_by_trace_id(trace_id)

            # TODO fix base model class so that all values aren't arrays
            if not self.TraceOutput.aberrant[0]:
                self.detect_pull(trace_id)

    def detect_pull(self, trace_id):

        # trace = self.PDM.find_by_trace_id(trace_id).to_dataframe()
        trace = self.PDM.find_with_pandas(trace_id)

        # pull out the useful things
        usefilter = trace['contact_filter'].isin([0]) & trace['retract_filter'].isin([1])
        force = np.asarray(trace['force_interference_corrected'][usefilter])
        dist = np.asarray(trace['distance'][usefilter])

        # calculate noise as 'reduced mass' of global and local noise
        noise = (
            (self.TraceOutput.noise_ref[0] * self.mean_noise) /
            (self.TraceOutput.noise_ref[0] + self.mean_noise)
        )

        # more cleaning
        dist, force = cleaners.remove_surface_points(dist, force)
        smoothed_force = cleaners.moving_average_smoother(force)

        # look for pulls
        DD = DropDetector(smoothed_force, dist, noise_ref=noise)

        # for now this is the best detection metric
        self.TraceOutput.pull_detect = [DD.spikedrop_present]
        if DD.spikedrop_present:
            self.TraceOutput.pull_dist = [np.amax(dist[DD.spikedrop_mask])]
            self.TraceOutput.pull_force = [-np.amin(force[DD.spikedrop_mask])]

        self.TOM.update(self.TraceOutput)
